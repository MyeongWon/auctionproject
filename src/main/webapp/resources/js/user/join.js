function formCheck() {
	var userId = document.getElementById("userId").value;
	if (userId.trim().length <= 0) {
		alert("아이디를 입력해주세요.");
		return false;
	}
	if (userId.replace(" ", "") != userId) {
		alert("아이디에 공백을 없애주세요.");
		return false;
	}
	if (userId.length < 3 || userId.length > 10) {
		alert("아이디는 3~10글자 사이로 입력해주세요");
		return false;
	} 
	
	var userPassword = document.getElementById("userPassword").value;
	var userPasswordChk = document.getElementById("userPasswordChk").value;
	if (userPassword.trim().length <= 0) {
		alert("비밀번호를 입력해주세요.");
		return false;
	}
	if (userPassword.replace(" ", "") != userPassword) {
		alert("비밀번호에 공백을 없애주세요.");
		return false;
	}
	if (userPassword.length < 3 || userPassword.length > 10) {
		alert("비밀번호는 3~10글자 사이로 입력해주세요");
		return false;
	} 
	if (userPassword != userPasswordChk) {
		alert("동일한 비밀번호를 입력해주세요");
		return false;
	} 
	
	var userNickname = document.getElementById("userNickname").value;
	if (userNickname.trim().length <= 0) {
		alert("닉네임을 입력해주세요.");
		return false;
	}
	if (userNickname.length < 3 || userNickname.length > 10) {
		alert("닉네임은 3~10글자 사이로 입력해주세요");
		return false;
	} 
	var userPhonenum = document.getElementById("userPhonenum").value;
	if (userPhonenum.trim().length <= 0) {
		alert("휴대전화를 입력해주세요.");
		return false;
	}
	var userEmail = document.getElementById("userEmail").value;
	if (userEmail.trim().length <= 0) {
		alert("이메일을 입력해주세요.");
		return false;
	}
	var userAddress = document.getElementById("detailAddress").value;
	if (userAddress.trim().length <= 0) {
		alert("주소를 입력해주세요.");
		return false;
	}

	return true;
}

function idCheck() {
	var userId = document.getElementById("userId").value;
	
	var idCheckWindow = window.open("/idCheck?userId=" + userId, "_blank", "width=400, height=320");
	
	// 새 창의 로딩이 끝난 후 함수를 실행시킴
	/*idCheckWindow.onload = function() {
		// 새 창의 입력란을 가리키는 html 요소
		var idCheckInput = idCheckWindow.document.getElementById("userId");
		
		// 회원가입 창에서 사용자가 입력한 ID값
		var userId = document.getElementById("userId").value;
		
		idCheckInput.value = userId;
	};*/
}
function formCheck2() {
	var userId = document.getElementById("userId").value;

	
	var userPassword = document.getElementById("userPassword").value;
	var userPasswordChk = document.getElementById("userPasswordChk").value;
	if (userPassword.trim().length <= 0) {
		alert("비밀번호를 입력해주세요.");
		return false;
	}
	if (userPassword.replace(" ", "") != userPassword) {
		alert("비밀번호에 공백을 없애주세요.");
		return false;
	}
	if (userPassword.length < 3 || userPassword.length > 10) {
		alert("비밀번호는 3~10글자 사이로 입력해주세요");
		return false;
	} 
	if (userPassword != userPasswordChk) {
		alert("동일한 비밀번호를 입력해주세요");
		return false;
	} 
	

	var userPhonenum = document.getElementById("userPhonenum").value;
	if (userPhonenum.trim().length <= 0) {
		alert("휴대전화를 입력해주세요.");
		return false;
	}
	var userEmail = document.getElementById("userEmail").value;
	if (userEmail.trim().length <= 0) {
		alert("이메일을 입력해주세요.");
		return false;
	}
	var userAddress = document.getElementById("detailAddress").value;
	if (userAddress.trim().length <= 0) {
		alert("주소를 입력해주세요.");
		return false;
	}

	return true;
}
