function idCheck() {
	var userId = document.getElementById("userId").value;
	
	if (userId.trim().length <= 0) {
		var resultStr = "검색하려는 ID를 입력하세요.";
		document.getElementById("result").innerHTML = resultStr;
		return false;
	}
	
	return true;
}

function useId() {
	var userId = document.getElementById("userId").value;
	
	window.opener.document.getElementById("userId").value = userId;
	
	window.close();
}

function erase() {
	document.getElementById("result").innerHTML = "";
};