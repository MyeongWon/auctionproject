function sellCheck() {
	var inputTitle = document.getElementById("inputTitle").value;
	
	if (inputTitle.trim().length <= 0) {
		alert("제목을 입력해주세요.");
		return;
	}
	
	var inputPriceStart = document.getElementById("inputPriceStart").value;
	
	if (inputPriceStart.trim().length <=0) {
		alert("경매 시작가를 입력해주세요.");
		return;
	}
	var inputPriceImmed = document.getElementById("inputPriceImmed").value;
	
	if (inputPriceImmed.trim().length <=0) {
		alert("즉시 구매가를 입력해주세요.");
		return;
	}
	var inputCategory = document.getElementById("inputCategory").value;
	
	if (inputCategory.trim().length <=0) {
		alert("카테고리를 선택해주세요.");
		return;
	}
	
	var inputContent = document.getElementById("inputContent").value;
	
	if (inputContent.trim().length <=0) {
		alert("설명을 입력해주세요.");
		return
	}
	
	document.getElementById("formTitle").value = inputTitle;
	document.getElementById("formPriceStart").value = inputPriceStart;
	document.getElementById("formPriceImmed").value = inputPriceImmed;
	
	document.getElementById("formCategory").value = inputCategory
	document.getElementById("formContent").value = inputContent;
	document.getElementById("sellForm").submit();
}