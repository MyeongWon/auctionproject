/**
 * 
 */

function auctionCheck() {
	
	var nowPrice = document.getElementById("nowPrice").innerHTML;
	var priceStart = document.getElementById("priceStart").value;
	var priceImmed = document.getElementById("formPriceImmed").value;
	var userPoint = document.getElementById("userPoint").innerHTML;


	var intNow = parseInt(nowPrice);
	var intStart = parseInt(priceStart);
	var intPoint = parseInt(userPoint);
	var intImmed = parseInt(priceImmed);
	

	if (intNow >= intStart) {
		alert("현재 가격보다 높은 가격을 제시해주세요")
		return false;
	}

	if (intStart > intPoint) {
		alert("포인트가 부족합니다")
		return false;
	}
	
	if (intStart >= intImmed) {
		alert("입찰가는 즉시 구매가보다 높을 수 없습니다")
		return false;
	}

	document.getElementById("formPriceStart").value = intStart;
	document.getElementById("formNowPrice").value = intNow;
	
	
	var params = $("#auctionForm").serialize();
	
	$.ajax(
		{
			type: "post",
			url: "auctionCheck",
			data: params
		}).done(function(msg){
			alert("입찰되었습니다");
			window.opener.location.reload();
			window.close();
			});
	

	return true;

}