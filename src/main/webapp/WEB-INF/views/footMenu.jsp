<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

  <!-- footer -->  
  <footer id="aa-footer">
    <!-- footer bottom -->
    <div class="aa-footer-top">
     <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="aa-footer-top-area">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <h3>Project</h3>
                  <ul class="aa-footer-nav">
                    <li><a>앉아서 경매하자 : 앉경</a></li>
                    <li><a>온라인 중고거래 사이트</a></li>
                    <li><a>경매기능 추가</a></li>
                    <li><a>포인트 제도 도입 등</a></li>
                    <li><a>열심히 만들었습니다!</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>team</h3>
                    <ul class="aa-footer-nav">
                      <li><a>광주무역회관 7층</a></li>
                      <li><a>SWDO 5기 1조</a></li>
                      <li><a></a></li>
                      <li><a></a></li>
                      <li><a></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>member</h3>
                    <ul class="aa-footer-nav">
                      <li><a>이명원</a></li>
                      <li><a>송다솔</a></li>
                      <li><a>정승현</a></li>
                      <li><a>홍지유</a></li>
                      <li><a></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>address</h3>
                    <address>
                      <p>아래 연락처로 연락하시면 안됩니다</p>
                      <p><span class="fa fa-phone"></span>062-0000-0000</p>
                      <p><span class="fa fa-envelope"></span>SWDO5-1@gmail.com</p>
                    </address>
                    <div class="aa-footer-social">
                      <a><span class="fa fa-facebook"></span></a>
                      <a><span class="fa fa-twitter"></span></a>
                      <a><span class="fa fa-google-plus"></span></a>
                      <a><span class="fa fa-youtube"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     </div>
    </div>
    <!-- footer-bottom -->
    <div class="aa-footer-bottom">
      <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="aa-footer-bottom-area">
            <div class="aa-footer-payment">
              <span class="fa fa-cc-mastercard"></span>
              <span class="fa fa-cc-visa"></span>
              <span class="fa fa-paypal"></span>
              <span class="fa fa-cc-discover"></span>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </footer>
  <!-- / footer -->

</body>
</html>