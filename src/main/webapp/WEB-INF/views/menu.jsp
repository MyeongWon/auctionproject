<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">

	
</script>

</head>
<body>
	
  <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <header id="aa-header">
    <!-- start header top  -->
    <div class="aa-header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-top-area">
              <div class="aa-header-top-right">
                <ul class="aa-head-top-nav-right">
                 <c:if test="${not empty sessionScope.userId}">
                  <li><a href="#"> ${sessionScope.userNickname}님 환영합니다.</a></li>
                  <li><a href="/myPage">마이 페이지</a></li>
                  <li class="hidden-xs"><a href="/wishlist">위시리스트</a></li>
                  <li><a href="/logout">로그아웃</a></li>
                 </c:if>
                 
                 <c:if test="${empty sessionScope.userId}">
                 <li><a href="/login">로그인</a></li>
                 </c:if>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="aa-header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-bottom-area">
              <!-- logo  -->
              <div class="aa-logo">
                <!-- Text based logo -->
                <a href="/">
                  <!-- <span class="fa fa-shopping-cart"></span> -->
                  <p>앉<strong>경</strong> <span>Your Auction Partner</span></p>
                </a>
                <!-- img based logo -->
                <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
              </div>
              <!-- / logo  -->
							<!-- search box -->
							<div class="aa-search-box">
								<form action="/search" method="post" onsubmit="return formCheck();">
									<div class="searchbox">
										<div class="header" onclick="searchClick()">
											<input onkeyup="filter()" type="text" id="searchKeyword" name="searchKeyword"
												placeholder="검색하세요.">
										</div>
									</div>
									<button type="submit">
									<span class="fa fa-search"></span>
									</button>
								</form>
							</div>

							<!-- / search box -->             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header bottom  -->
  </header>
  <!-- / header section -->
  <!-- menu -->
  <section id="menu">
    <div class="container">
      <div class="menu-area">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>          
          </div>
          <div class="navbar-collapse collapse">
            <!-- Left nav -->
            <ul class="nav navbar-nav">
              <li><a href="/">홈으로</a></li>
              <li><a href="/select">상품 조회</a></li>            
              <li><a href="/sell">상품 판매</a></li>
              
              <li><a href="#">커뮤니티 <span class="caret"></span></a>
                <ul class="dropdown-menu">                
                  <li><a href="/event">이벤트</a></li>
                  <li><a href="/community">요청게시판</a></li>                
                  <li><a href="/free">자유게시판</a></li>                
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>       
    </div>
  </section>
</body>
</html>