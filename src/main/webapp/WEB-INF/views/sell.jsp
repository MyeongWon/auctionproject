<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>
    
    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
   
    <style> input:focus, textarea:focus{outline: none; } </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
  <script type="text/javascript">
	  function readURL(input) {
		  if (input.files && input.files[0]) {
		  var reader = new FileReader();
		  reader.onload = function (e) {
		  $('#blah').attr('src', e.target.result);
		  }
		  reader.readAsDataURL(input.files[0]);
		  }
	  }

  </script>
  
  <style type="text/css">
  	.z{
  		margin-right: 50px;
  		width: 256px;
  		height: 225px
  	}
  
  </style>
  
  </head>
  <body> 
	<%@ include file="/WEB-INF/views/menu.jsp" %>

  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="resources/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>상품 판매</h2>
        <ol class="breadcrumb">
          <li><a>YOUR AUCTION PARTNER '앉경'</a></li>                   
        </ol>
      </div>
     </div>
   </div>
  </section>
  
  <!-- / catg header banner section -->
<section id="aa-myaccount">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <div class="aa-myaccount-area"> 
    	<!-- <form action="sell" method="post" id="sellForm" enctype="multipart/form-data">
			<input type="hidden" id="formTitle" name="title">
			<input type="hidden" id="formPriceStart" name="priceStart">
			<input type="hidden" id="formPriceImmed" name="priceImmed">
			<input type="hidden" id="formCategory" name="category">
			<input type="hidden" id="formContent" name="content">
			<input type="file" name="image" accept="image/*">
		</form> -->
	  	<table> <!-- style="margin-left: 200px;" -->
	  		<tr>
	  			<td><img id="blah" alt="" src="resources/img/noname.png" class="z"></td>
	  			<td rowspan="2">
	  				<table class="table" border="3" style="margin-left: 100px;">
						<tr>
							<th class="m2">상품명</th>
							<td align=left>
								<input type=text id = "inputTitle" name="text" size="20" style="width:100%; border: 0;">
							</td>
						</tr>
						<tr>
							<th class="m2">입찰가</th>
							<td align=left>
								<input type=text id= "inputPriceStart" name="text" size="20" style="width:100%; border: 0;">
							</td>
						</tr>
						<tr>
							<th class="m2">즉시구매가</th>
							<td align=left>
								<input class="" type=text id="inputPriceImmed" name="text" size="20" style="width:100%; border: 0;">
							</td>
						</tr>
						<tr>
							<th class="m2">카테고리 설정</th>
							<td align=left>
							<select id="inputCategory" style="width:100%; border: 0;">
								<option value="" SELECTED>
								::카테고리 설정::</option>
								<option value="디지털/가전">디지털/가전</option>
								<option value="가구/인테리어">가구/인테리어</option>
								<option value="유아용품">유아용품</option>
								<option value="생활/가공식품">생활/가공식품</option>
								<option value="스포츠/레져">스포츠/레져</option>
								<option value="여성패션/잡화">여성패션/잡화</option>
								<option value="남성패션/잡화">남성패션/잡화</option>
								<option value="게임/취미">게임/취미</option>
								<option value="뷰티/미용">뷰티/미용</option>
								<option value="동식물 용품">동식물 용품</option>
								<option value="도서/티켓/기타">도서/티켓/기타</option>
							</select>
							</td>
						</tr>
						<tr>
							<th class="m2">상품 소개</th>
							<td align=left>
							<textarea rows=7 cols=50 id ="inputContent" name="content" style="width:100%; border: 0; resize: none;"></textarea>
							</td>
						</tr>
				</table>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<form action="sell" method="post" id="sellForm" enctype="multipart/form-data" runat="server">
						<input type="hidden" id="formTitle" name="title">
						<input type="hidden" id="formPriceStart" name="priceStart">
						<input type="hidden" id="formPriceImmed" name="priceImmed">
						<input type="hidden" id="formCategory" name="category">
						<input type="hidden" id="formContent" name="content">
						<label class="myfile" for="input-file" style="margin-left: 60px;">
						  이미지 첨부하기
						</label>
						<input type="file" name="image" id="input-file" accept="image/*" onchange="readURL(this);" style="display:none;">
					</form>
	  			</td>
	  		</tr>
	  	</table>
	  </div>
	 </div>
	</div>
 </div>
</section>	
		<div style="text-align: center; margin-bottom: 50x;">
			<button class="aa-browse-btn" type="button" onclick="sellCheck();">상품 등록</button>
			 &nbsp;<button class="aa-browse-btn" type="reset" onclick="location='/sell';">취소</button>
		</div>
		<br><br>
	<%@ include file="/WEB-INF/views/footMenu.jsp" %> 

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 
  <!-- 판매 폼 체크 자바스크립트 -->
  <script type="text/javascript" src="resources/js/item/sell.js"></script>

  </body>
</html>