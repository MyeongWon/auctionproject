<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
	<title>앉아서 경매하자 ::앉경 - ${board.boardTitle }</title>

	<!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    
  	<style> input:focus, textarea:focus{outline: none; } </style>
  
</head>
	<body>
	
	<%@ include file="/WEB-INF/views/menu.jsp" %>

<!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="resources/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>상세보기</h2>
        <ol class="breadcrumb">
          <li><a>YOUR AUCTION PARTNER '앉경'</a></li>                   
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

<section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">  
			<form action="communityWrite" method = "post" id="writeForm" enctype="multipart/form-data" onsubmit="return writeCheck();">
		        <table border="2" class="table" style="text-align: center;">
          				<%-- <td><input type="hidden" name="boardNum" value="${board.boardNum}"></td>  --%>
                        
                        <tr>
                        <td>작성자</td>
                        <td><input type="text" name="boardId" value="${board.boardId}" readonly="readonly" size="20" style="width:100%; border: 0;"></td>
                        </tr>
 
                        <tr>
                        <td>제목</td>
                        <td><input type="text" name="boardTitle" value="${board.boardTitle}" readonly="readonly" size="20" style="width:100%; border: 0;"></td>
                        </tr>
 
                        <tr>
                        <td>내용</td>
                        <td><textarea cols=85 rows=15 readonly="readonly" style="width:100%; border: 0; resize: none;">${board.boardContent }</textarea></td>
                        </tr>
 
               </table>
               	<div style="text-align: center;">
               	<c:if test="${sessionScope.userId eq board.boardId}">
               		<input type="button" value="수정" class="aa-browse-btn" onclick="location.href='/communityUpdate?boardNum=${board.boardNum}';">
               		<input type="button" value="삭제" class="aa-browse-btn" onclick="location.href='/communityDelete?boardNum=${board.boardNum}';">
             	</c:if>
             
               		<input type="button" value="뒤로" class="aa-browse-btn" onclick="location.href='/community';">
             	</div>
			  </form>
			 </div>
			</div>
		</div>
	</div>
</section>

<%@ include file="/WEB-INF/views/footMenu.jsp" %> 


  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 

</body>
</html>