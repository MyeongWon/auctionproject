<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>
    <script type="text/javascript" src="/resources/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="/resources/js/item/oneSelect.js"></script>

    
    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
  
  </head>
  <body> 
	<%@ include file="/WEB-INF/views/menu.jsp" %> 
 
   <!-- product category -->
  <section id="aa-product-details">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-product-details-area">
            <div class="aa-product-details-content">
              <div class="row">
                <!-- Modal view slider -->
                <div class="col-md-5 col-sm-5 col-xs-12">                              
                  <div class="aa-product-view-slider">                                
                    <div id="demo-1" class="simpleLens-gallery-container">
                      <div class="simpleLens-container">
                        <div class="simpleLens-big-image-container"><img src="<c:url value='/upimg/${item.aucImage }'/>" class="simpleLens-big-image"></a></div>
                      </div>
                      <!-- <div class="simpleLens-thumbnails-container">
                          <a data-big-image="resources/img/view-slider/medium/polo-shirt-1.png" data-lens-image="img/view-slider/large/polo-shirt-1.png" class="simpleLens-thumbnail-wrapper" href="#">
                            <img src="resources/img/view-slider/thumbnail/polo-shirt-1.png">
                          </a>                                    
                          <a data-big-image="resources/img/view-slider/medium/polo-shirt-3.png" data-lens-image="img/view-slider/large/polo-shirt-3.png" class="simpleLens-thumbnail-wrapper" href="#">
                            <img src="resources/img/view-slider/thumbnail/polo-shirt-3.png">
                          </a>
                          <a data-big-image="resources/img/view-slider/medium/polo-shirt-4.png" data-lens-image="img/view-slider/large/polo-shirt-4.png" class="simpleLens-thumbnail-wrapper" href="#">
                            <img src="resources/img/view-slider/thumbnail/polo-shirt-4.png">
                          </a>
                      </div> -->
                    </div>
                  </div>
                </div>
                <!-- Modal view content -->
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="aa-product-view-content">
                    <h3>${item.aucTitle }</h3>
                    
                    <form action="buyImmediately" method="post" id="buyForm">
						<input type="hidden" id="formAucNum" name="aucNum">
						<input type="hidden" id="formPriceImmed" name="priceImmed">
					</form>
	
                    <input type="hidden" id="aucNum" value="${item.aucNum }">
                    <input type="hidden" id="userId" value="${sessionScope.userId }">
                    
                    <div class="aa-price-block">
                      <span class="aa-product-view-price">현재 경매진행금액 : </span><span >${item.aucPriceStart }</span><br>
                      <span class="aa-product-view-price">시작금액 : <span id=aucPriceStart>${item.aucPriceStart }</span></span><br>
                      <span class="aa-product-view-price">즉시구입가 : <span id=aucPriceImmed>${item.aucPriceImmed }</span></span><br>
                    <c:if test="${empty gradeList}">
						<span class="aa-product-view-price">판매자 별점 : 0</span>
					</c:if>
					<c:if test="${not empty gradeList}">
						<span class="aa-product-view-price">판매자 별점 : ${userGrade / userCheck}</span>
					</c:if>
                      
                      
                      
                    </div>
                    <p>상세내용 : ${item.aucContent }</p>

                    <div class="aa-prod-view-bottom">
                      
                    	<%-- <a class="aa-add-to-cart-btn" href='javascript:wishlistCheck();' onclick="/wishlistcheck?aucNum=${item.aucNum }">찜</a> --%>
						<c:if test='${isnull eq "null" }'>
						<a class="aa-add-to-cart-btn" onclick='javascript:wishlistCheck(${item.aucNum});'>찜</a>
						</c:if>
						<c:if test='${isnull eq "notnull" }'>
                      	<a class="aa-add-to-cart-btn" onclick='javascript:wishlistDelete(${item.aucNum });'>찜 해제</a>
						</c:if>
                      <a class="aa-add-to-cart-btn" onclick="auctionCheck();">입찰하기</a>
					  <a class="aa-add-to-cart-btn" onclick="buyCheck();">구매하기</a>
					
                      <%-- <a class="aa-add-to-cart-btn" href="/buyImmediately?aucNum=${item.aucNum }&priceImmed=${item.aucPriceImmed }">구매하기</a> --%>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / product category -->
  
	<%@ include file="/WEB-INF/views/footMenu.jsp" %> 

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 
  <!-- 구매 버튼 클릭 시 체크 자바스크립트 -->
  <script type="text/javascript" src="resources/js/item/check.js"></script>


  </body>
</html>