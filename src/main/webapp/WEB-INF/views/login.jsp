<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/user/login.js"></script>
    
    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  

  </head>
  <body> 
   <%@ include file="/WEB-INF/views/menu.jsp" %> 

  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-login">
                <h4>로그인</h4>
                <c:if test="${empty userNickname}">
                 <form method="post" action="" class="aa-login-form" onsubmit="return formCheck();">
                  <label for="">회원 ID<span>*</span></label>
                   <input type="text" placeholder="아이디를 입력해주세요" id="userId" name="userId">
                   <label for="">비밀번호<span>*</span></label>
                    <input type="password" placeholder="비밀번호를 입력해주세요" id="userPassword" name="userPassword">
                    <button type="submit" class="aa-browse-btn">로그인</button>
                    <label class="rememberme" for="rememberme"></label>
                    <p></p>&nbsp;
                    <p class="aa-lost-password">아이디/패스워드가 없으신가요?&ensp; <a href="/join" style="color: #ff6666">회원가입</a></p>
                    <br>
                  </form>
                  </c:if>
                  <a href="javascript:kakaoLogin();"><img src="/resources/img/ic-kakao-login.png" width="300px" height="auto" /></a>
                  <script src="https://developers.kakao.com/sdk/js/kakao.js"></script>
                  <script>
                  window.Kakao.init('8bc524774d2f05ce9a241d82aeeef032');

                  function kakaoLogin() {
                  	window.Kakao.Auth.login({
                  		scope: 'profile, account_email',
                  		success: function(authObj) {
                  			window.Kakao.API.request({
                  				url: '/v2/user/me',  
                  				success: res => {
                  					const kakao_account = res.kakao_account;
                  					const kakao_prof = kakao_account.profile
                  					$.ajax({
                  						url:"/kakaoLogin",
                  						type:"get",
                  						data:{
                  							kakaoEmail:kakao_account.email
                  							,kakaoNickname:kakao_prof.nickname
                  						},	
                  						contentType:"application/json;charset=utf-8;"
                  					}).done(function(msg){
                  						console.log(kakao_account.email);
                  						if(msg=="socialJoin"){
                  							alert("회원이 아닙니다. 회원가입 페이지로 이동합니다.");
                  							location.href="/"+msg+"?userNickname="+kakao_prof.nickname+"&userEmail="+kakao_account.email;
                  						}else if(msg=="redirect:/" ){
											alert("로그인합니다");
                  							location.replace("/");
                  						}
                  							
                  					})
                  					.fail(function(e){
                  						alert("로그인에 실패했습니다. 처음부터 다시 시작하세요");
                  						console.log(e);
                  					});
                  			   }
                  			});

                  			}
                  		});
                  }
                  </script>
                  <c:if test="${not empty userNickname}">
                  ${userNickname } 님, 환영합니다.
                  <br>
                  <a href="/logout">로그아웃</a>
                  </c:if>
                </div>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->
  
   <%@ include file="/WEB-INF/views/footMenu.jsp" %> 

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 

  </body>
</html>