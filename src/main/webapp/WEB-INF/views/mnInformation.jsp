<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>
    
    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
	<script type="text/javascript" src="/resources/jquery-3.5.1.min.js"></script>
    <script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
	<script>
    //본 예제에서는 도로명 주소 표기 방식에 대한 법령에 따라, 내려오는 데이터를 조합하여 올바른 주소를 구성하는 방법을 설명합니다.
    function sample4_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 표시한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var roadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 참고 항목 변수

                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('sample4_postcode').value = data.zonecode;
                document.getElementById("sample4_roadAddress").value = roadAddr;
                document.getElementById("sample4_jibunAddress").value = data.jibunAddress;
                
                // 참고항목 문자열이 있을 경우 해당 필드에 넣는다.
                if(roadAddr !== ''){
                    document.getElementById("sample4_extraAddress").value = extraRoadAddr;
                } else {
                    document.getElementById("sample4_extraAddress").value = '';
                }

                var guideTextBox = document.getElementById("guide");
                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
                    guideTextBox.innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';
                    guideTextBox.style.display = 'block';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
                    guideTextBox.innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';
                    guideTextBox.style.display = 'block';
                } else {
                    guideTextBox.innerHTML = '';
                    guideTextBox.style.display = 'none';
                }
            }
        }).open();
    }
    
    function formCheck() {
    	var userId = document.getElementById("userId").value;
    	if (userId.trim().length <= 0) {
    		alert("아이디를 입력해주세요.");
    		return false;
    	}
    	if (userId.replace(" ", "") != userId) {
    		alert("아이디에 공백을 없애주세요.");
    		return false;
    	}
    	if (userId.length < 3 || userId.length > 10) {
    		alert("아이디는 3~10글자 사이로 입력해주세요");
    		return false;
    	} 
    	
    	var userPassword = document.getElementById("userPassword").value;
    	var userPasswordChk = document.getElementById("userPasswordChk").value;
    	if (userPassword.trim().length <= 0) {
    		alert("비밀번호를 입력해주세요.");
    		return false;
    	}
    	if (userPassword.replace(" ", "") != userPassword) {
    		alert("비밀번호에 공백을 없애주세요.");
    		return false;
    	}
    	if (userPassword.length < 3 || userPassword.length > 10) {
    		alert("비밀번호는 3~10글자 사이로 입력해주세요");
    		return false;
    	} 
    	if (userPassword != userPasswordChk) {
    		alert("동일한 비밀번호를 입력해주세요");
    		return false;
    	} 
    	
    	var userNickname = document.getElementById("userNickname").value;
    	if (userNickname.trim().length <= 0) {
    		alert("닉네임을 입력해주세요.");
    		return false;
    	}
    	if (userNickname.length < 3 || userNickname.length > 10) {
    		alert("닉네임은 3~10글자 사이로 입력해주세요");
    		return false;
    	} 
    	var userPhonenum = document.getElementById("userPhonenum").value;
    	if (userPhonenum.trim().length <= 0) {
    		alert("휴대전화를 입력해주세요.");
    		return false;
    	}
    	var userEmail = document.getElementById("userEmail").value;
    	if (userEmail.trim().length <= 0) {
    		alert("이메일을 입력해주세요.");
    		return false;
    	}
    	var userAddress = document.getElementByName("detailAddress").value;
    	if (userAddress.trim().length <= 0) {
    		alert("주소를 입력해주세요.");
    		return false;
    	}

    	return true;
    }

    function idCheck() {
    	var userId = document.getElementById("userId").value;
    	
    	var idCheckWindow = window.open("/idCheck?userId=" + userId, "_blank", "width=300, height=400");
    	
    }
	</script>

	<style type="text/css">
	
	.p {color: red;}
	.wrapper {margin-left: 400px;}
	
	table {margin-left: 100px;}

	.i {width: 400px; height: 35px;}
	
	h4{font-size: 33px; font-weight: bold; margin-left: 100px; margin-bottom: 40px;}
	
	</style>
	
  </head>
  <body> 
	<%@ include file="/WEB-INF/views/menu.jsp" %>

	
<pre style="background-color: white; border: 0;">

</pre>
	<h4>회원정보 수정</h4><hr>
	<form action="/mnInformation" method="post" enctype="multipart/form-data" onsubmit="return formCheck();">
	<table>
            <tr>
                <td><span class="p">*</span>회원 ID</td>
                <td><input type="text" class="i" id="userId" name="userId" value="${user.userId}">
                <!-- <input type="button" value="ID중복검사" onclick="idCheck();"></td> -->
            </tr>
            <tr>
                <td colspan="3"><hr /></td>
            </tr>
            <tr>
                <td><span class="p">*</span>비밀번호</td>
                <td><input type="password" class="i" placeholder="비밀번호를 입력해주세요" id="userPassword" name="userPassword"></td>
            </tr>
             <tr>
                <td colspan="3"><hr /></td>
            </tr>
            <tr>
                <td><span class="p">*</span>비밀번호 확인</td>
                <td><input type="password" class="i" id="userPasswordChk" name="userPasswordChk"></td>
            </tr>
            <tr>
                <td colspan="3"><hr /></td>
            </tr>
            <tr>
                <td><span class="p">*</span>이 름</td>
                <td><input type="text" class="i" id="userNickname" name="userNickname" value="${user.userNickname}"></td>
            </tr>
            <tr>
                <td colspan="3"><hr /></td>
            </tr>
            <tr>
                <td><span class="p">*</span>전화번호('-'없이 번호만 입력해주세요) </td>
                <td><input type="tel" class="i" id="userPhonenum" name="userPhonenum" value="${user.userPhonenum}"></td>
            </tr>
            <tr>
                <td colspan="3"><hr /></td>
            </tr>
            <tr>
                <td><span class="p">*</span>이메일</td>
                <td><input type="email" class="i" id="userEmail" name="userEmail" value="${user.userEmail}"></td>
            </tr>
            <tr>
                <td colspan="3"><hr /></td>
            </tr>
            <tr>
                <td><span class="p">*</span>주 소</td>
                <td>
                    <input type="text" id="sample4_postcode" class="i" placeholder="우편번호" readonly="readonly" name="postcode">&ensp;
					<input type="button" onclick="sample4_execDaumPostcode()" value="우편번호 찾기" class="bb-browse-btn"><br><br>
					<input type="text" id="sample4_roadAddress" class="i" placeholder="도로명주소" readonly="readonly" name="roadAddress"><br>
					<input type="hidden" id="sample4_jibunAddress" class="i" placeholder="지번주소" readonly="readonly" name="jibunAddress"><br>
					<span id="guide" style="color:#999;display:none"></span>
					<input type="text" id="sample4_detailAddress" class="i" placeholder="상세주소" id="detailAddress" name="detailAddress">
					<p></p>
					<input type="text" id="sample4_extraAddress" class="i" placeholder="참고항목" readonly="readonly" name="extraAddress">
                </td>
            </tr>
        </table>
		<br><br>
		<div class="wrapper">
			<input type="submit" value="수정" class="aa-browse-btn">
			<input type="reset" value="취소" class="aa-browse-btn" onclick="location.href='/myPage';">
		</div>
		
		</form>
<pre style="background-color: white; border: 0;">

</pre>

	<%@ include file="/WEB-INF/views/footMenu.jsp" %> 

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 

  </body>
</html>