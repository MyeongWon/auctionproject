<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
	<title>회원탈퇴</title>
    
   
   <script type="text/javascript">
   function delForm() {
		var userPassword = document.getElementById("userPassword").value;
		
		if (userPassword.trim().length <= 0) {
			alert("비밀번호를 입력해주세요.");
			return false;
		}
	}
   </script>
   
   <script type="text/javascript">
   
   function passCheck(userPassword){
	var msg = "${msg}";
	alert(msg);
	if(msg == "비밀번호가 맞습니다.") {
		location.href='/mnInformation?userPassword='+userPassword;
		document.getElementById('passChk').submit();
	}
	else {
		alert(msg);
	}
   	
   }
	</script>
   

  </head>
 <body>
			<form action="/passChk" id="passChk" method="post" onsubmit="return delForm();">
					<label for="userId">아이디</label>
					<input type="text" id="userId" name="userId" value="${user.userId}" readonly="readonly"/>
				
					<label for="userPassword">패스워드</label>
					<input type="password" id="userPassword" name="userPassword" />
			
					<label for="userNickname">성명</label>
					<input type="text" id="userNickname" name="userNickname" value="${user.userNickname}" readonly="readonly"/>
					
					<input type = "submit" value="수정" onclick="passChk();">
					<input type = "button" value="회원탈퇴" onclick="location.href='/userDelete?userId=${user.userId}';">
					
			</form>
	</body>
</html>