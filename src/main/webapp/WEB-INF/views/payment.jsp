<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>
    <script type="text/javascript" src="/resources/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="resources/js/user/join.js"></script>
    
    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body> 
	<%@ include file="/WEB-INF/views/menu.jsp" %>

  <!-- / catg header banner section -->
  
  <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-register">                 
                 <h4>결제 페이지</h4>
                 <form action="/join" class="aa-login-form" method="post" onsubmit="return formCheck();">
                    <label for="">회원 ID<span>*</span></label>
                    <input type="text" placeholder="아이디를 입력해주세요" id="userId" name="userId">
                    <input type="button" value="ID중복검사" onclick="idCheck();">
                    <br><br>
                    <label for="">비밀번호<span>*</span></label>
                    <input type="password" placeholder="비밀번호를 입력해주세요" id="userPassword" name="userPassword">
                    <label for="">비밀번호 확인<span>*</span></label>
                    <input type="password" placeholder="다시 한번 비밀번호를 입력해주세요" id="userPasswordChk" name="userPasswordChk">
                    <label for="">이름<span>*</span></label>
                    <input type="text" placeholder="이름을 입력해주세요" id="userNickname" name="userNickname">
                    <label for="">전화번호('-'없이 번호만 입력해주세요)<span>*</span></label>
                    <input type="text" placeholder="전화번호를 입력해주세요" id="userPhonenum" name="userPhonenum">
                    <label for="">이메일<span>*</span></label>
                    <input type="text" placeholder="이메일을 입력해주세요" id="userEmail" name="userEmail">
                    <label for="">주소<span>*</span></label>
                    
                    <input type="text" placeholder="우편번호" readonly="readonly">
                    <input type="button" value="우편번호 찾기"><br><br />
                    <input type="text" placeholder="도로명/지번 주소" readonly="readonly">
                    <br /><br /><input type="text" placeholder="상세 주소" name="userAddress">
                	</td>
                    
                	<div class="wrapper">
						<button class="aa-browse-btn" type="submit">가입</button>
						<button class="aa-browse-btn" type="reset">취소</button>
					</div>                 
                  </form>
                </div>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 
	<%@ include file="/WEB-INF/views/footMenu.jsp" %>    

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 

  </body>
</html>