<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>앉아서 경매하자 ::앉경</title>

	    <!-- Font awesome -->
	    <link href="resources/css/font-awesome.css" rel="stylesheet">
	    <!-- Bootstrap -->
	    <link href="resources/css/bootstrap.css" rel="stylesheet">   
	    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
	    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
	    <!-- Product view slider -->
	    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
	    <!-- slick slider -->
	    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
	    <!-- price picker slider -->
	    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
	    <!-- Theme color -->
	    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
	    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
	    <!-- Top Slider CSS -->
	    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">
	
	    <!-- Main style sheet -->
	    <link href="resources/css/style.css" rel="stylesheet">    
	
	    <!-- Google Font -->
	    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	    
		<script type="text/javascript" src="/resources/jquery-3.5.1.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		
		<script type="text/javascript" src="/resources/js/item/payment.js"></script>
		
		<!-- iamport.payment.js -->
		<script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>

	<style type="text/css">
		.form {
			margin: auto;
			text-align: center;
			padding: 20px 10px;
			width: 50%;
			border: 1px solid;
			font-size: 25px;
		}
	
	</style>

	</head>

<body>

	<%@ include file="/WEB-INF/views/menu.jsp" %>

  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="resources/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>포인트 충전하기</h2>
        <ol class="breadcrumb">
          <li><a>YOUR AUCTION PARTNER '앉경'</a></li>                   
        </ol>
      </div>
     </div>
   </div>
  </section>


<!-- <form action="/pointCharge" method="post"> -->
	<input type="hidden" id="userId" value="${user.userId }">
	<input type="hidden" id="userEmail" value="${user.userEmail }">
	<input type="hidden" id="userAddress" value="${user.userAddress }">
	<input type="hidden" id="aucTitle" value="포인트 충전">
	<input type="hidden" id="userPhone" value="${user.userPhonenum }">
	<input type="hidden" id="aucNum" value="${currentTime }">
 <pre style="background-color: white; border: 0;">
 
 </pre>	
<%-- 	<table>
		<tr>
		<td>결제할 금액 :</td>
		<td>	
		<select id="aucPrice" name="amount">
	    <option value="5" selected>5000원</option>
	    <option value="10">10000원</option>
	    <option value="20" >20000원</option>
	    <option value="50">50000원</option>
		</select>
		</td>
		</tr>
		
		<tr><td>현재 잔액 :</td><td>${user.userPoint }원</td></tr>
		
	</table>  --%>
	<form class="form">
		결제할 금액 : 
		<select id="aucPrice" name="amount">
	    <option value="5" selected>5000원</option>
	    <option value="10">10000원</option>
	    <option value="20" >20000원</option>
	    <option value="50">50000원</option>
		</select>&emsp;
		현재 잔액 : ${user.userPoint }원
	</form><br>
		<div style="text-align: center;">
			<input type="submit" value="충전하기" class="aa-browse-btn" onclick="charge()" >
		</div>
		
 <pre style="background-color: white; border: 0;">
 
 </pre>
<!-- </form>
 -->
 
 	<%@ include file="/WEB-INF/views/footMenu.jsp" %> 

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 
 
</body>

</html>