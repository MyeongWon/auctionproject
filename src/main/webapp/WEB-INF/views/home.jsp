<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>앉아서 경매하자 ::앉경</title>

<!-- Font awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css" href="resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher" href="resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>
	<%@ include file="/WEB-INF/views/menu.jsp"%>

	<!-- / menu -->
	<!-- Start slider -->
	<section id="aa-slider">
		<div class="aa-slider-area">
			<div id="sequence" class="seq">
				<div class="seq-screen">
					<ul class="seq-canvas">
						<!-- single slide item -->
						<li>
							<div class="seq-model">
								<img data-seq src="resources/img/slider/1.jpg"
									alt="Men slide img" />
							</div>
							<div class="seq-title" >
								<span data-seq>Review Event</span>
								<h2 data-seq>리뷰 쓰고 커피 받자!</h2>
								<p data-seq>선착순 100명!</p>
								<a data-seq class="aa-shop-now-btn aa-secondary-btn" href='/getEvent?boardNum=5'>자세히 보기</a>
							</div>
						</li>

						<!-- single slide item -->
						<li>
							<div class="seq-model">
								<img data-seq src="resources/img/slider/2.jpg"
									alt="Wristwatch slide img" />
							</div>
							<div class="seq-title">
								<span data-seq>Save Up to 30% Off</span>
								<h2 data-seq>4월 봄!봄!봄! 이벤트</h2>
								<p data-seq>봄에 피는 벚꽃처럼 나도 예뻐지나 봄</p>
								<a data-seq class="aa-shop-now-btn aa-secondary-btn" href='/getEvent?boardNum=4'>자세히 보기</a>
							</div>
						</li>
						<!-- single slide item -->
						<li>
							<div class="seq-model">
								<img data-seq src="resources/img/slider/3.jpg"
									alt="Women Jeans slide img" />
							</div>
							<div class="seq-title">
								<span data-seq>무료 배송 Event</span>
								<h2 data-seq>조이멀티 검색하고 무료배송 받자!</h2>
								<p data-seq>우주복, 상하복, 수면조끼, 유아신발은 배송이 무료!</p>
								<a data-seq class="aa-shop-now-btn aa-secondary-btn" href='/getEvent?boardNum=6'>자세히 보기</a>
							</div>
						</li>
						
					</ul>
				</div>
				<!-- slider navigation btn -->
				<fieldset class="seq-nav" aria-controls="sequence"
					aria-label="Slider buttons">
					<a type="button" class="seq-prev" aria-label="Previous"><span
						class="fa fa-angle-left"></span></a> <a type="button" class="seq-next"
						aria-label="Next"><span class="fa fa-angle-right"></span></a>
				</fieldset>
			</div>
		</div>
	</section>



	<!-- / slider -->
	<!-- popular section -->
	<section id="aa-popular-category">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="aa-popular-category-area">
							<!-- start product navigation -->
							<ul class="nav nav-tabs aa-products-tab">
								<li class="active"><a href="#popular" data-toggle="tab">인기상품</a></li>
								<li><a href="#featured" data-toggle="tab">최저가상품</a></li>
								<li><a href="#category" data-toggle="tab">카테고리</a></li>

							</ul>
							<!-- Tab panes -->
							<div class="tab-content">

								<!-- Start men popular category -->
								<div class="tab-pane fade in active" id="popular">
									<ul class="aa-product-catg aa-popular-slider">
										<!-- start single product item -->
										<c:forEach var="pl" items="${popularityList}"
											varStatus="status" end="7">
											<li>
												<figure>
													<a class="aa-product-img" href='/oneSelect?aucNum=${pl.aucNum}'>
														<img src="<c:url value='/upimg/${pl.aucImage }'/>" alt="img" width="250" height ="300">
													</a>
													<a class="aa-add-card-btn" href="#"><span
														class="fa fa-shopping-cart"></span>Add To Cart</a>
													<figcaption>
														<h4 class="aa-product-title">
															<a href='/oneSelect?aucNum=${pl.aucNum}'>${pl.aucTitle}</a>
														</h4>
														<span class="aa-product-price">${pl.aucPriceStart}</span><span
															class="aa-product-price"></span>
													</figcaption>
												</figure>
												<div class="aa-product-hvr-content">
													<a href="#" data-toggle="tooltip" data-placement="top"
														title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
													<a href="#" data-toggle="tooltip" data-placement="top"
														title="Compare"><span class="fa fa-exchange"></span></a> <a
														href="#" data-toggle2="tooltip" data-placement="top"
														title="Quick View" data-toggle="modal"
														data-target="#quick-view-modal"><span
														class="fa fa-search"></span></a>
												</div>
											</li>
										</c:forEach>

									</ul>
									<a class="aa-browse-btn" href="/select">전체 상품 보기 <span
										class="fa fa-long-arrow-right"></span></a>
								</div>
								<!-- / popular product category -->




								<!-- start featured product category -->
								<div class="tab-pane fade" id="featured">
									<ul class="aa-product-catg aa-featured-slider">
										<!-- start single product item -->
										<c:forEach var="fl" items="${featuredList}" varStatus="status"
											end="7">
											<li>
												<figure>
												 
													<a class="aa-product-img" href='/oneSelect?aucNum=${fl.aucNum}' >
														<img src="<c:url value='/upimg/${fl.aucImage }'/>" alt="img" width="250" height ="300">
													</a>
													<a class="aa-add-card-btn" href="#"><span
														class="fa fa-shopping-cart"></span>Add To Cart</a>
													<figcaption>
														<h4 class="aa-product-title">
															<a href='/oneSelect?aucNum=${fl.aucNum}'>${fl.aucTitle}</a>
														</h4>
														<span class="aa-product-price">${fl.aucPriceStart}</span><span
															class="aa-product-price"></span>
													</figcaption>
												</figure>
												<div class="aa-product-hvr-content">
													<a href="#" data-toggle="tooltip" data-placement="top"
														title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
													<a href="#" data-toggle="tooltip" data-placement="top"
														title="Compare"><span class="fa fa-exchange"></span></a> <a
														href="#" data-toggle2="tooltip" data-placement="top"
														title="Quick View" data-toggle="modal"
														data-target="#quick-view-modal"><span
														class="fa fa-search"></span></a>
												</div>
											</li>
										</c:forEach>
									</ul>
									<a class="aa-browse-btn" href="/select">전체 상품 보기 <span
										class="fa fa-long-arrow-right"></span></a>
								</div>
								<!-- / featured product category -->

								<!-- start featured product category -->
								<div class="tab-pane fade" id="category">
									<ul class="aa-product-catg aa-featured-slider">
										<!-- start single product item -->
										<li>
											<figure>
												<a class="aa-product-img" href="/getDigital"> <img
													src="resources/img/category/digital.PNG" width="250"
													height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getInterior"> <img
													src="resources/img/category/interior.PNG" width="250"
													height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getInfant"> <img src="resources/img/category/infant.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getLife"> <img src="resources/img/category/life.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getSports"> <img src="resources/img/category/sports.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getWoman"> <img src="resources/img/category/woman.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getMan"> <img src="resources/img/category/man.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getGame"> <img src="resources/img/category/game.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getBeauty"> <img src="resources/img/category/beauty.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getAnimal"> <img src="resources/img/category/animal.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
										<li>
											<figure>
												<a href="/getBook"> <img src="resources/img/category/book.PNG"
													width="250" height="300">
												</a>
												<a class="aa-add-card-btn" href="">+ 더보기</a>
											</figure>
										</li>
									</ul>
									<div style="display: block; text-align: center;">
										<a class="aa-browse-btn" href="/select">전체 상품 보기 <span
											class="fa fa-long-arrow-right"></span></a>
									</div>
									<!-- <div class="img-cover"></div> -->
								</div>
								<!-- / featured product category -->



							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- / popular section -->


	<%@ include file="/WEB-INF/views/footMenu.jsp"%>

	<!-- jQuery library -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="resources/js/bootstrap.js"></script>
	<!-- SmartMenus jQuery plugin -->
	<script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
	<!-- SmartMenus jQuery Bootstrap Addon -->
	<script type="text/javascript"
		src="resources/js/jquery.smartmenus.bootstrap.js"></script>
	<!-- To Slider JS -->
	<script src="resources/js/sequence.js"></script>
	<script src="resources/js/sequence-theme.modern-slide-in.js"></script>
	<!-- Product view slider -->
	<script type="text/javascript"
		src="resources/js/jquery.simpleGallery.js"></script>
	<script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
	<!-- slick slider -->
	<script type="text/javascript" src="resources/js/slick.js"></script>
	<!-- Price picker slider -->
	<script type="text/javascript" src="resources/js/nouislider.js"></script>
	<!-- Custom js -->
	<script src="resources/js/custom.js"></script>

</body>
</html>
