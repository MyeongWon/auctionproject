<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<title>앉아서 경매하자 ::앉경</title>
	<link href="resources/css/idCheck.css" rel="stylesheet">
	<script type="text/javascript" src="/resources/js/user/idCheck.js"></script>
	<script type="text/javascript">
	window.onload = function() {
		document.getElementById("userId").value = "${userId }";

		var resultStr = "${result }";
		if (resultStr.length > 10) {
			var resultDiv = document.getElementById("result");

			var str = "";str += "<br><br>";
			str += "<input type='button' value='사용하기' onclick='useId();'";
			str += "class = myBt>"
			resultDiv.innerHTML += str;
		}
	}
	</script>
</head>
<body>
	<h2>ID 중복검사</h2>
	<p>* 사용하려는 ID를 입력하고 검색 버튼을 클릭하세요.</p><br>
	<form action="/idCheck" method="post" onsubmit="return idCheck();">
		<input type="text" id="userId" name="userId" onkeyup='erase();'>
		<input type="submit" value="검색" class="myB">
	</form><br>
	<div id="result">${result }</div>
</body>
</html>