<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>star</title>
<link rel="stylesheet" type="text/css" href="resources/css/star.css">

</head>
<body>
<div class="wrap">
    <h1>후기</h1>
    <form name="reviewform" class="reviewform" method="post" action="/star">
        <input type="hidden" name="rate" id="rate" value="0"/>
        <p class="title_star">별점을 남겨주세요.</p>
        <input type="hidden" value="${aucSellId}" name="aucSellId">
        <input type="hidden" value="${aucNum}" name="aucNum"> 
 
        <div class="review_rating">
            <div class="warning_msg">별점을 선택해 주세요.</div>
            <div class="rating">
                <!-- 해당 별점을 클릭하면 해당 별과 그 왼쪽의 모든 별의 체크박스에 checked 적용 -->
                <input type="radio" name="rating" id="rating1" value="1" class="rate_radio" title="1점">
                <label for="rating1">1점</label>
                <input type="radio" name="rating" id="rating2" value="2" class="rate_radio" title="2점">
                <label for="rating2">2점</label>
                <input type="radio" name="rating" id="rating3" value="3" class="rate_radio" title="3점" >
                <label for="rating3">3점</label>
                <input type="radio" name="rating" id="rating4" value="4" class="rate_radio" title="4점">
                <label for="rating4">4점</label>
                <input type="radio" name="rating" id="rating5" value="5" class="rate_radio" title="5점">
                <label for="rating5">5점</label>
            </div>
        </div>
      
        <div class="cmd">
            <input type="submit" name="save" id="save" value="등록">
        </div>
    </form>
</div>


</body>
</html>