<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>
    
    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
   
   <script type="text/javascript">
   	function search() {
		var searchType = document.getElementById("searchType").value;
		var searchWord = document.getElementById("searchWord").value;
		
		if (searchWord.trim().length <= 0) {
			alert("검색어를 입력해주세요.");
			
			return;
		}
		
		document.getElementsByName("searchType")[0].value = searchType;
		document.getElementsByName("searchWord")[0].value = searchWord;
		
		document.getElementById("searchForm").submit();
	}
	</script>
   

  </head>
  <body> 
	<%@ include file="/WEB-INF/views/menu.jsp" %>
<!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="resources/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>자유게시판</h2>
        <ol class="breadcrumb">
          <li><a>YOUR AUCTION PARTNER '앉경'</a></li>                   
        </ol>
      </div>
     </div>
   </div>
  </section>
<pre style="background-color: white; border: 0;">

</pre>
<div class="container">
	<table class="table table-striped">
		<thead>
			<tr>
				<th style="width: 100px;">글 번호</th>
				<th style="width: 300px;">제목</th>
				<th style="width: 200px;">작성자</th>
				<th style="width: 200px;">작성일</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${empty list }">
				<tr>
					<td colspan="4">게시글이 없습니다.</td>
				</tr>
			</c:if>
			<c:if test="${not empty list }">
				<c:forEach var="fb" items="${list }">
					<tr>
						<td>${fb.boardNum }</td>
						<td>
							<a href="/getFree?boardNum=${fb.boardNum }">
								${fb.boardTitle }
							</a>
						</td>
						<td>${fb.boardId }</td>
						<td>${fb.boardDate }</td>
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3" style="text-align: left">
					<select id="searchType">
						<option value="boardTitle">제목</option>
						<option value="boardContent">내용</option>
						<option value="boardId">작성자</option>
						<option value="boardNum">게시글 번호</option>
					</select>
					<input type="text" id="searchWord">
					<input type="button" value="검색" class="bb-browse-btn" onclick="search();">
				</td>
				<td style="text-align: right;">
				<c:if test="${not empty sessionScope.userId}">
					<input type="button" value="글 쓰기" class="bb-browse-btn" onclick="location.href='freeWrite';">
				</c:if>
				</td>
			</tr>
		</tfoot>
	</table>
</div><br>
	
	<form action="free" method="get" id="searchForm">
		<input type="hidden" name="searchType">
		<input type="hidden" name="searchWord">
	</form>

	<%@ include file="/WEB-INF/views/footMenu.jsp" %>     

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 

  </body>

</html>