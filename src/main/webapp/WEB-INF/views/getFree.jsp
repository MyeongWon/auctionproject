<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
	<title>앉아서 경매하자 ::앉경 - ${board.boardTitle }</title>

	<!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>    
  
   <script type="text/javascript">
   var boardNum = ${board.boardNum};
   
    $(document).ready(function(){
    	getBoard();
    });

    function getBoard(){
		$.ajax({
			url: "/getReply", //컨트롤러의 주소
			type: "GET", //통신의 방식을 설정
			dataType: "json",
			data: {"boardNum" : boardNum},
			success: function(data){
					console.log(data);
					var list = "";
					for(var i=0;i<data.length;i++){
						list+="<tr><td>"+data[i].writer+"&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"+"</td>"
						+"<td>"+data[i].content+ "&emsp;&emsp;&emsp;&emsp;&emsp;"+"</td>"
						+"<td>"+data[i].regDate +"&emsp;&emsp;"+"</td>"+"<br>"
						+"<c:if test="${sessionScope.userId eq board.boardId}">"
						+'<td><input type="button" id="'+data[i].rno+'" value="삭제" class="myPage" onclick="deleteReply(this.id);"></td></tr>' 
						+"</c:if>";
					}
					document.getElementById('tb').innerHTML = list;
				}/* ,
				error:function(request,status,error){
			        alert("code = "+ request.status + " message = " + request.responseText + " error = " + error); // 실패 시 처리
			       },
			     complete : function(data) {
			                 //  실패했어도 완료가 되었을 때 처리
			        } */
		});
	}
    
    function writeReply(){
    	var content = document.getElementById("content").value;
    	 $.ajax({
             url : "replyWrite",
             type : "POST",
             data :{
            	 "bno" : boardNum,
            	 "content" : content    	 
             },
             success : function(){
             	getBoard();
             },
             error : function(error){console.log(error);}
             
         });
    	 document.getElementById('content').value="";
    };
    
    function deleteReply(id){           
            $.ajax({
                url : "deleteReply",
                type : "POST",
                data :{"rno" : id},
                success : function(){
                	getBoard();
                },
                error : function(error){console.log(error);}
                
            });
	}; 
    </script> 
    
  <style> 
  input:focus, textarea:focus{outline: none; } 
  
  h4{
  	color: #ff6666;
  	text-align: left;
  }
  
  /* .form{
  	margin-left: 50px;
  } */
  
  .id{
	text-align: center; 
	padding: 5px 5px; 
	width: 100px;
	color: #ff6666;
	border: 1px solid white;
  }
  
  .tb2{
  	margin: auto;
  	/* border: 2px solid; */
  }
  
  .td1{
  	padding: 5px 5px;
  	font-size: 25px;
  	font-weight: bold;
  	font-style: italic;
  }
  
  .coment{
  	margin: auto;
  	padding: 15px 40px;
  	/* font-weight: bold; */
  	font-size: 20px;
  }
  
  </style>

</head>
<body>

	<%@ include file="/WEB-INF/views/menu.jsp" %>

<!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="resources/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>상세보기</h2>
        <ol class="breadcrumb">
          <li><a>YOUR AUCTION PARTNER '앉경'</a></li>                   
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

<section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">  
			<form action="communityWrite" method = "post" id="writeForm" enctype="multipart/form-data" onsubmit="return writeCheck();">
		        <table border="2" class="table" style="text-align: center;">
          				<%-- <tr><td><input type="hidden" name="boardNum" value="${board.boardNum}"></td></tr>  --%>
                        
                        <tr>
                        <td>작성자</td>
                        <td><input type="text" name="boardId" value="${board.boardId}" readonly="readonly" size="20" style="width:100%; border: 0;"></td>
                        </tr>
 
                        <tr>
                        <td>제목</td>
                        <td><input type="text" name="boardTitle" value="${board.boardTitle}" readonly="readonly" size="20" style="width:100%; border: 0;"></td>
                        </tr>
 
                        <tr>
                        <td>내용</td>
                        <td><textarea cols=85 rows=15 readonly="readonly" style="width:100%; border: 0; resize: none;">${board.boardContent}</textarea></td>
                        </tr>
 
               </table>
               	<div style="text-align: center;">
               	<c:if test="${sessionScope.userId eq board.boardId}">
               		<input type="button" value="수정" class="aa-browse-btn" onclick="location.href='/freeUpdate?boardNum=${board.boardNum}';">
               		<input type="button" value="삭제" class="aa-browse-btn" onclick="location.href='/freeDelete?boardNum=${board.boardNum}';">
             	</c:if>
             	
               		<input type="button" value="뒤로" class="aa-browse-btn" onclick="location.href='/free';">
            	</div>
             </form>
			 </div>
		   </div>
		</div>
	</div>
</section>

		<form action="replyWrite" method="post" class="form">
		<table class="tb2">
			<tr>
			<td class="td1">
			<input type="text" id="writer" name="writer" value="${sessionScope.userId }" readonly="readonly" class="id">
			</td>
			<td>
			<textarea rows="3" cols="100" id="content" name="content" style="resize: none;"></textarea>
			</td>
			<td>			
			<input type="hidden" id="bno" name="bno" value="${board.boardNum }">
			
			<c:if test="${not empty sessionScope.userId}">
	      		&emsp;<input type="button" value="작성" class="aa-browse-btn" onclick="writeReply();">
	    	</c:if>
	    	</td>
	    	</tr>
	    	</table>
		</form>
        
        <br>
        <table id="tb" class="coment"></table>
        
        <%@ include file="/WEB-INF/views/footMenu.jsp" %> 


  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 
</body>
</html>
