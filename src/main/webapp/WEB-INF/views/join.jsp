<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>
    <script type="text/javascript" src="/resources/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="resources/js/user/join.js"></script>
    <script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
	<script>
    //본 예제에서는 도로명 주소 표기 방식에 대한 법령에 따라, 내려오는 데이터를 조합하여 올바른 주소를 구성하는 방법을 설명합니다.
    function sample4_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 표시한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var roadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 참고 항목 변수

                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('sample4_postcode').value = data.zonecode;
                document.getElementById("sample4_roadAddress").value = roadAddr;
                document.getElementById("sample4_jibunAddress").value = data.jibunAddress;
                
                // 참고항목 문자열이 있을 경우 해당 필드에 넣는다.
                if(roadAddr !== ''){
                    document.getElementById("sample4_extraAddress").value = extraRoadAddr;
                } else {
                    document.getElementById("sample4_extraAddress").value = '';
                }

                var guideTextBox = document.getElementById("guide");
                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
                    guideTextBox.innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';
                    guideTextBox.style.display = 'block';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
                    guideTextBox.innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';
                    guideTextBox.style.display = 'block';
                } else {
                    guideTextBox.innerHTML = '';
                    guideTextBox.style.display = 'none';
                }
            }
        }).open();
    }
	</script>
    
    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body> 
	<%@ include file="/WEB-INF/views/menu.jsp" %>

  <!-- / catg header banner section -->
  
  <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-register">                 
                 <h4>회원가입</h4>
                 <form action="/join" class="aa-login-form" method="post" onsubmit="return formCheck();">
                    <label for="">회원 ID<span>*</span></label>
                    <input type="text" placeholder="아이디를 입력해주세요" id="userId" name="userId">
                    <input type="button" value="ID중복검사" class="myPage" onclick="idCheck();">
                    <br><br>
                    <label for="">비밀번호<span>*</span></label>
                    <input type="password" placeholder="비밀번호를 입력해주세요" id="userPassword" name="userPassword">
                    <label for="">비밀번호 확인<span>*</span></label>
                    <input type="password" placeholder="다시 한번 비밀번호를 입력해주세요" id="userPasswordChk" name="userPasswordChk">
                    <label for="">이름<span>*</span></label>
                    <input type="text" placeholder="이름을 입력해주세요" id="userNickname" name="userNickname">
                    <label for="">전화번호( ' - ' 없이 번호만 입력해주세요)<span>*</span></label>
                    <input type="text" placeholder="전화번호를 입력해주세요" id="userPhonenum" name="userPhonenum">
                    <label for="">이메일<span>*</span></label>
                    <input type="text" placeholder="이메일을 입력해주세요" id="userEmail" name="userEmail">
                    <label for="">주소<span>*</span></label>
                    

                    <input type="text" id="sample4_postcode" placeholder="우편번호" readonly="readonly" id="postcode" name="postcode">
					<input type="button" onclick="sample4_execDaumPostcode()" value="우편번호 찾기" class="myPage"><p></p>
					<input type="text" id="sample4_roadAddress" placeholder="도로명주소" readonly="readonly" id="roadAddress" name="roadAddress">
					<input type="hidden" id="sample4_jibunAddress" placeholder="지번주소" readonly="readonly" id="jibunAddress" name="jibunAddress">
					<span id="guide" style="color:#999;display:none"></span>
					<input type="text" id="sample4_detailAddress" placeholder="상세주소" id="detailAddress" name="detailAddress">
					<input type="text" id="sample4_extraAddress" placeholder="참고항목" readonly="readonly" id="extraAddress" name="extraAddress">
                	<div class="wrapper">
						<button class="aa-browse-btn" type="submit">가입</button>
						<button class="aa-browse-btn" type="reset">취소</button>
					</div>                 
                  </form>
                </div>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 
	<%@ include file="/WEB-INF/views/footMenu.jsp" %> 

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 

  </body>
</html>