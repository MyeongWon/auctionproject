<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<script type="text/javascript">
  		
  	</script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>앉아서 경매하자 ::앉경</title>

    <script type="text/javascript" src="/resources/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="/resources/js/item/oneSelect.js"></script> 


    <!-- Font awesome -->
    <link href="resources/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="resources/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="resources/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="resources/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="resources/css/style.css" rel="stylesheet">    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    
    <style type="text/css">
    
    .myTable {
	border: 1px solid;
	margin: auto;
	margin-bottom: 30px;
	width: 100%;
	}
	
	.t-f {
		padding: 10px 30px;
		font-weight: bold;
		font-size: 20px;
	}
	
	.nm {
		font-size: 20px;
	}
	
	.myPage {
		display: inline-block;
		font-size: 15px;
		padding: 5px 5px;
		background-color: white;
		color: #ff6666;
		text-align: center;
	   	border: 1px solid #ff6666;
	  	/* border-radius: 55%; */
	  	-webkit-transition: all 0.5s;
	 	-moz-transition: all 0.5s;
	  	-ms-transition: all 0.5s;
	  	-o-transition: all 0.5s;
	  	transition: all 0.5s;
	  	width: 100%;
	}
	
	.myPage span {
	  margin-left: 5px;
	}
	
	.myPage:hover, .myPage:focus {
		display: inline-block;
		padding: 5px 5px;
		color: white;
		background-color: #ff6666;
	}
	
	.div {
		float:left;
		width: 50%;
	}
    </style>
    
    
  </head>
  <body> 
	<%@ include file="/WEB-INF/views/menu.jsp" %>
  
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="resources/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>마이페이지</h2>
        <ol class="breadcrumb">
          <li><a>YOUR AUCTION PARTNER '앉경'</a></li>                   
        </ol>
      </div>
     </div>
   </div>
  </section>


<section id="cart-view">
	<div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="cart-view-area">
	    <table class="myTable">
	    	<h7>프로필</h7>
	    	<p class="myP"><a href='/mnInformation'>회원정보 수정>></a></p>
	    	<tr>

	    		<td style="font-weight: bold;" class="t-f">이름</td>
	    		<td style="font-weight: bold;" class="nm" id="userId">${user.userId }</td>
				<td style="font-weight: bold;" class="t-f">포인트</td>
				<td style="font-weight: bold;" class="nm">${user.userPoint }</td>
				<td style="font-weight: bold;" class="t-f">평점</td>
				<c:if test="${empty gradeList }">
				<td style="font-weight: bold;" class="nm">0</td>
				</c:if>
				<c:if test="${not empty gradeList }">
				<td style="font-weight: bold;" class="nm">${userGrade /userCheck }</td>

				</c:if>
			</tr>
	    </table>
	    <div class="div"><a class="myPage" href='/nhSetting'>내 동네 확인하기</a></div>
	    <div class="div"><a class="myPage" href='javascript:void(0);' onclick="location.href='/pointCharge';">포인트 충전하기</a></div>
		<p><br><br><br></p>
	    <table class="table">
	    	<h7>구매내역</h7>
	    	<p class="myP"><a href='/purchase'>더보기>></a></p>
	    	<tr style="font-weight: bold;">
				<td>&emsp;</td>
				<td>상품명</td>
				<td>카테고리</td>
				<td>시작가</td>
				<td>구매가</td>
				<td>구매날짜</td>
				<td></td>
			</tr>
			<c:forEach var="sl" items="${purchaseList }" begin="0" end="2">
            <tr> 
            	<td><a href='javascript:void(0);' onclick="toOneSelect(${sl.aucNum })"></a></td>
                <td><a class="aa-cart-title" href='javascript:void(0);' onclick="toOneSelect(${sl.aucNum })">${sl.aucTitle }</a></td>
                <%-- <td>${sl.aucNum }</td> --%>
                <td>${sl.aucCategory }</td>
                <td>${sl.aucPriceStart }</td>
                <td>${sl.aucPriceImmed }</td>
                <td>${sl.aucDate }</td>         
                <td><a href="/star?aucNum=${sl.aucNum }">별점주기</a></td>               
            </tr>
                 </c:forEach>  
	    </table><br><br>
	    <table class="table">
	    	<h7>판매내역</h7>
	    	<p class="myP"><a href='/sale'>더보기>></a></p>
	    	<tr style="font-weight: bold;">
				<td>&emsp;</td>
				<td>상품명</td>
				<td>카테고리</td>
				<td>시작가</td>
				<td>현재가격</td>
				<td>등록날짜</td>
				<td></td>
			</tr>
			<tr>
				<c:forEach var="pr" items="${saleList }" begin="0" end="2">
                      <tr> 
                        <td><a href='javascript:void(0);' onclick="toOneSelect(${pr.aucNum })"></a></td>
                        <td><a class="aa-cart-title" href='javascript:void(0);' onclick="toOneSelect(${pr.aucNum })">${pr.aucTitle }</a></td>
                        <td>${pr.aucCategory }</td>
                        <td>${pr.aucPriceStart }</td>
                        <td>${pr.aucPriceImmed }</td>
                        <td>${pr.aucDate }</td> 
                        <td>&nbsp;&nbsp;&nbsp;</td>                       
                      </tr>
                 </c:forEach>  
			</tr>
	    </table><br><br>
	</div>
	</div>
	</div>
	</div>
</section>




	<%@ include file="/WEB-INF/views/footMenu.jsp" %> 

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="resources/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="resources/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="resources/js/sequence.js"></script>
  <script src="resources/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="resources/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="resources/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="resources/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="resources/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="resources/js/custom.js"></script> 

  </body>
</html>