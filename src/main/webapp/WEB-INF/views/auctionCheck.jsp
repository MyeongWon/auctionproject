<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>앉아서 경매하자 ::앉경</title>
	<script type="text/javascript" src="/resources/jquery-3.5.1.min.js"></script>
	
	<script type="text/javascript">
	window.onload = function() {
		document.getElementById("priceStart").value = "${priceStart }";

	}
	</script>
	
	<script type="text/javascript" src="resources/js/item/auctionCheck.js"></script>
	
    <link href="resources/css/auctionCheck.css" rel="stylesheet">  
    
</head>
<body>
	<h1>입 찰 하 기</h1><hr color="#ff6666;" size="10px;"><br>
	<h2>내 포인트</h2><hr>
	<p id=userPoint>${user.userPoint } 원</p><br>
	<h2>현재 가격</h2><hr>
	<p id=nowPrice>${priceStart } 원</p><br>
	<h2>입찰 가격</h2><hr>
	<p><input type="number" id="priceStart"> 원</p><br>
	
	<form action="/auctionCheck" method="post" id="auctionForm" >
		<input type="hidden" id="formNowPrice" name ="nowPrice">
		<input type="hidden" id="formPriceStart" name="priceStart">
		<input type="hidden" id="formPriceImmed" name="priceImmed" value="${priceImmed }">
	</form><br><br>
	
	<div><input type="button" value="입찰하기 >> " class="myB" onclick="auctionCheck();"></div>
</body>
</html>