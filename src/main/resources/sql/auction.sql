drop table user_data cascade constraint;
drop table user_grade cascade constraint;
drop table auction cascade constraint;
drop table board_request cascade constraint;
drop table board_etc cascade constraint;
drop table board_event cascade constraint;
drop table board_free cascade constraint;
drop sequence usersq;
drop sequence reqboardsq; 
drop sequence auctionsq;

create sequence usersq NOCACHE;
create sequence reqboardsq NOCACHE;
create sequence auctionsq NOCACHE;
create sequence freeboardsq NOCACHE;
create sequence eventboardsq NOCACHE;

Create table user_data(
user_num number primary key,
user_id varchar2(50) not null unique,
user_password varchar2(24) not null,
user_nickname varchar2(30) not null,
user_address varchar2(120) not null,
user_phonenum number(11) not null,
user_email varchar2(50) not null,
user_membership number(1) default 0,
constraint user_ck check(user_membership in(0,1)),
user_point number default 0,
user_nh varchar2(50)
);

create table user_grade(
user_num number,
user_check number default 0,
user_grade number(2,1) default 0, 
constraint user_ck2 check(user_check between 0 and 5),
constraint usernum_fk FOREIGN KEY(user_num) REFERENCES user_data(user_num)
);
alter table user_grade add constraint usernum_fk FOREIGN KEY(user_num) REFERENCES user_data(user_num);


Create table auction(
auc_num number primary key,
auc_category varchar2(30) not null,
auc_title varchar2(120) not null,
auc_date date default sysdate,
auc_limit date not null,
auc_pricestart number not null,
auc_priceimmed number,
auc_sell_id varchar2(50) not null,
auc_purchase_id varchar2(50),
auc_content varchar2(2000) not null,
auc_image BLOB,
auc_status number(1),
CONSTRAINTS user_fk FOREIGN KEY(auc_sell_id) REFERENCES user_data(user_id),
constraint auc_st check(auc_status between 1 and 3),
auc_popular number default 0 
);

Create table board_request(
board_num number primary key,
board_id varchar2(30) not null,
board_title varchar2(200) not null,
board_content varchar2(4000) not null,
board_date date default sysdate,
constraint bod_fk foreign key(board_id) references user_data(user_id)
);

Create table board_event(
board2_num number primary key,
board2_id varchar2(30) not null,
board2_title varchar2(200) not null,
board2_content varchar2(4000) not null,
board2_date date default sysdate, 
constraint bod2_fk foreign key(board2_id) references user_data(user_id)
);

Create table board_free(
board3_num number primary key,
board3_id varchar2(30) not null,
board3_title varchar2(200) not null,
board3_content varchar2(4000) not null,
board3_date date default sysdate, 
constraint bod3_fk foreign key(board3_id) references user_data(user_id)
);

commit;




insert into user_data values(
usersq.nextval,
'admin',
'1234',
'������',
'�ƹ�����',
12345678,
'aa@asd.as',
0,
0);

insert into auction(
auc_num,
auc_title,
auc_category,
auc_date,
auc_limit,
auc_pricestart,
auc_priceimmed,
auc_sell_id,
auc_content,
auc_image,
auc_status,
auc_popular
)values(
auctionsq.nextval,
'��',
'������ s8�˴ϴ�',
sysdate,
'2021-04-29',
20000
,0
,'admin'
,'������S8�˴ϴ� 2�������Ϳ�'
,null
,1
,0
);
insert into auction(
auc_num,
auc_title,
auc_category,
auc_date,
auc_limit,
auc_pricestart,
auc_priceimmed,
auc_sell_id,
auc_content,
auc_image,
auc_status,
auc_popular
)
values(
auctionsq.nextval,
'��',
'�� �˴ϴ�',
sysdate,
'2021-04-29',
200000
,0
,'admin'
,'�� �˴ϴ� 20�����Ϳ�'
,null
,1
,0
);


commit;

select * from user_data;
select * from auction;

create table myReply (
rno number primary key
, bno number not null
, content varchar2(2000) not null
, writer varchar2(30) not null
, regDate date default sysdate
, constraint bno_fk foreign key(bno) references board_free(board3_num)
, constraint writer_fk foreign key(writer) references user_data(user_id)
);

drop table myReply;

create sequence myReply_seq;

select * from myReply;

commit;

