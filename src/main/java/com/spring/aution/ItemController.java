package com.spring.aution;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import org.springframework.web.multipart.MultipartFile;


import com.spring.aution.service.ItemService;
import com.spring.aution.service.UserService;
import com.spring.aution.vo.GradeVO;
import com.spring.aution.vo.ItemVO;
import com.spring.aution.vo.UserVO;
import com.spring.aution.util.FileService;

@Controller
public class ItemController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private static final String upload_path = "C:\\SpringFileUpload\\";

	@Autowired
	private ItemService service;
	
	
	@Autowired
	private UserService userService;

	
	@RequestMapping(value = "/sell", method = RequestMethod.GET)
	public String sell() {
		return "sell";
	}
	
	@RequestMapping(value = "/sell", method = RequestMethod.POST)
	public String sell(String title, String priceStart, String priceImmed, MultipartFile image, String category, String content, HttpSession session) throws Exception 
	{
		ItemVO vo = new ItemVO();
		String returnUrl;
		
		FileService fileSV = new FileService();
		String orgFileName = image.getOriginalFilename();
		String fileName = fileSV.saveFile(image, upload_path);

		
		
		vo.setAucTitle(title);
		vo.setAucPriceStart(Integer.parseInt(priceStart));
		vo.setAucPriceImmed(Integer.parseInt(priceImmed));
		vo.setAucSellId((String)session.getAttribute("userId"));
		if(fileName != null) {			
			vo.setAucImage(fileName);
		}
		vo.setAucCategory(category);
		vo.setAucContent(content);
		
		System.out.println(vo);
		boolean result = service.sell(vo);
		
		if(result) {
			logger.info("데이터 입력 성공");
			returnUrl= "redirect:/select";
		} else {
			logger.info("데이터 입력 실패");
			
			returnUrl= "redirect:/select";
		}
		

		
		return returnUrl;
	}
	
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public String select(Model model) {
		logger.info("select get method 실행");
		ArrayList<ItemVO> list = service.getList(); 
		
		//경매 시간 완료 시 로직
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		for(ItemVO itemList: list) {
			
			String limitDate = itemList.getAucLimit();
			String sellId = itemList.getAucSellId();
			int priceStart = itemList.getAucPriceStart();
			String bidId = itemList.getAucBidId();
			int aucNum = itemList.getAucNum();
			
			long nowTime = System.currentTimeMillis();
			String stringNowTime = dateFormat.format(new Date(nowTime));
			
			if (bidId != null) {
				try {
					Date changeLimitDate = dateFormat.parse(limitDate);
					Date changeNowTime = dateFormat.parse(stringNowTime);

					boolean timeResult = changeLimitDate.after(changeNowTime);

					if (timeResult == false) {
						// 입찰자가 구매자로 변경, 판매 상태로 전환
						service.setBidComplete(aucNum, bidId);
						// 판매자 포인트 증가
						userService.increasePoint(sellId, priceStart);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(bidId == null){
				try {
					Date changeLimitDate = dateFormat.parse(limitDate);
					Date changeNowTime = dateFormat.parse(stringNowTime);

					boolean timeResult = changeLimitDate.after(changeNowTime);

					if (timeResult == false) {
						bidId = "";
						// 입찰자가 구매자로 변경, 판매 상태로 전환
						service.setBidComplete(aucNum, bidId);;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}			
		}		
		
		ArrayList<ItemVO> updateList = service.getList();
		model.addAttribute("list", updateList);			

		return "select";
	}
	
	@RequestMapping(value = "/oneSelect", method = RequestMethod.GET)
	public String oneSelect(int aucNum, Model model, HttpSession session, HttpServletResponse response) {
		logger.info("oneselect get method 실행");

		String id = (String)session.getAttribute("userId");
		
		ItemVO result = service.getOne(aucNum);
		
		String fileName = result.getAucImage();
		
		
//		try {
//			response.setHeader("Content-Disposition", " attachment;filename="+ URLEncoder.encode(img, "UTF-8"));
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}		
		
		
		// 저장된 파일 경로
//		String fullPath = upload_path + fileName;

		// 서버의 파일을 읽을 입력 스트림과 클라이언트에게 전달할 출력스트림
		FileInputStream filein = null;
		ServletOutputStream fileout = null;

//		try {
//			filein = new FileInputStream(fullPath);
//			fileout = response.getOutputStream();
//
//			// Spring의 파일 관련 유틸
//			FileCopyUtils.copy(filein, fileout);
//			filein.close();
//			fileout.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		int sessionAucNum = result.getAucNum();
		
		System.out.println(result);
		UserVO userResult = userService.getUser(id); 
		service.increasePopular(aucNum);

//		model.addAttribute("fullpath",fullPath);

		if(id != "") {
			boolean result2  = service.getWishlistNum(id, aucNum);
			if (result2) {
				//찜 테이블에 찜이 없음
				model.addAttribute("isnull", "null");
			}else {
				//찜 테이블에 찜을 해놓음
				model.addAttribute("isnull" ,"notnull");
			}
		}
		else {
			model.addAttribute("isnull", "plzlogin");
		}
		service.increasePopular(aucNum);
		
		
		
		//별점띄우기
		String aucSellId = service.getSellId(aucNum);
		
		ArrayList<GradeVO> gradeList = userService.getGradeList(aucSellId);
		int userGrade = 0;
		if(gradeList==null) {
			System.out.println("나와라");
		} else {
		
		for(int i=0; i<gradeList.size(); i++) {
			userGrade += gradeList.get(i).getUserGrade();
		}
		model.addAttribute("userGrade", userGrade);
		
		int userCheck = 0;
		for(int i=0; i<gradeList.size(); i++) {
			userCheck += gradeList.get(i).getUserCheck();
		}
		model.addAttribute("userCheck", userCheck);
		}
		
		model.addAttribute("gradeList", gradeList);
		
		
		model.addAttribute("item", result);						 
		model.addAttribute("user", userResult);
		
		session.setAttribute("aucNum", sessionAucNum);
		
		return "oneSelect";
	}
	
	@RequestMapping(value = "/auctionCheck", method = RequestMethod.GET)
	public String auctionCheck(int priceStart, Model model, HttpSession session) {
		logger.info("auctionCheck(get) 메서드 실행.");

		String userId = (String)session.getAttribute("userId");
		UserVO user = userService.getUser(userId);
		int aucNum = (Integer) session.getAttribute("aucNum");
		ItemVO item = service.getOne(aucNum);
		int priceImmed = item.getAucPriceImmed();
		
		model.addAttribute("priceImmed", priceImmed);
		model.addAttribute("priceStart", priceStart);
		model.addAttribute("user", user);
		model.addAttribute("aucNum", aucNum);
		
		return "auctionCheck";
	}
	
	@ResponseBody
	@RequestMapping(value = "/auctionCheck", method = RequestMethod.POST)
	public String auctionCheck(int priceStart, int nowPrice, HttpSession session) {
		logger.info("auctionCheck(post) 메서드 실행.");
		String returnUrl ="";

		
		int aucNum = (Integer) session.getAttribute("aucNum");
		String userId = (String) session.getAttribute("userId");
		ItemVO item = service.getOne(aucNum);
		String bidId = item.getAucBidId();
		
		//입찰 가격 변경, 입찰자 등록
		boolean result = service.setAuctionStatus(aucNum, priceStart, userId);
		//새 입찰자 포인트 감소
		boolean result2 = service.setNewBuyerPoint(userId, priceStart);
		//기존 입찰자 포인트 반환
		boolean result3 = service.setFormerBuyerPoint(bidId, nowPrice);
				
		if(result) {
			logger.info("데이터 입력 성공");
			returnUrl= "redirect:/select";
			
		} else {
			logger.info("데이터 입력 실패");	
			returnUrl= "redirect:/oneSelect";
		}
		
		if(result2) {
			logger.info("데이터 입력 성공");
			returnUrl= "redirect:/select";
			
		} else {
			logger.info("데이터 입력 실패");	
			returnUrl= "redirect:/oneSelect";
		}
		
		if(result3) {
			logger.info("데이터 입력 성공");
			returnUrl= "redirect:/select";
			
		} else {
			logger.info("데이터 입력 실패");	
			returnUrl= "redirect:/oneSelect";
		}
		
		return returnUrl;
	}


	@RequestMapping(value = "/buyImmediately", method = RequestMethod.POST)
	public String buyImmediately(int aucNum, int priceImmed, HttpSession session) {
		String id = (String)session.getAttribute("userId");
		String sellerId = service.getOne(aucNum).getAucSellId();
		
		String returnUrl;
		boolean result = false;
		boolean result2 = false;
		
		
		if(id == null) {
			logger.info("데이터 입력 실패");
			
			returnUrl= "redirect:/oneSelect";
		}
		
		
		else {
			//구매자 포인트 감소
			result = userService.subtractPoint(id, priceImmed);
			//판매자 포인트 증가
			result2 = userService.increasePoint(sellerId, priceImmed);
		}
	
		if(result) {
			logger.info("데이터 입력 성공");
			returnUrl= "redirect:/select";
		} else {
			logger.info("데이터 입력 실패");
			
			returnUrl= "redirect:/oneSelect";
		}
		
		if(result2) {
			logger.info("데이터 입력 성공");
			returnUrl= "redirect:/select";
		} else {
			logger.info("데이터 입력 실패");
			
			returnUrl= "redirect:/oneSelect";
		}
		
		//판매 상태로 변환
		service.changeStatusSelled(id, aucNum);
		
		return returnUrl;
	}
    
    
    @RequestMapping(value = "/pointCharge", method = RequestMethod.GET)
	public String pointCharge(Model model,HttpSession session) {
    	
		logger.info("pointCharge get method 실행");

    	String userId = session.getAttribute("userId").toString();
    	
    	UserVO user = userService.getUser(userId);
    	long time = System.currentTimeMillis();
    	SimpleDateFormat simpl = new SimpleDateFormat("yyyyMMddhhmmss");
    	String currentTime = simpl.format(time);
    	
    	logger.info(currentTime);
    	model.addAttribute("user", user);
    	model.addAttribute("currentTime", currentTime);

		return "pointCharge";
	}
    @ResponseBody
    @RequestMapping(value = "/pointCharge", method = RequestMethod.POST)
	public String pointCharge(int amount, HttpSession session, Model model) {
    	String str, msg = null;
    	String userId = session.getAttribute("userId").toString();
    	boolean flag = false;
    	
    	System.out.println();
    	boolean result = service.pointCharge(amount,userId);
 
    	if(result) {msg = "결제성공";}
    	else {msg = "결제실패";}
    	
    	return msg;
    	
    }
    
    
    @RequestMapping(value = "/search", method = RequestMethod.POST)
   	public String search(String  searchKeyword, Model model) {
       	ArrayList<ItemVO> list = new ArrayList<ItemVO>();
		list = service.search(searchKeyword);
		model.addAttribute("list", list);
		
		System.out.println(searchKeyword);
		System.out.println(list);
      
       	return "select";
       	
       }
    
    @RequestMapping(value = "/bottomSearch", method = RequestMethod.GET)
   	public String bottomSearch(String searchCategory, String searchWord, Model model) {
       	ArrayList<ItemVO> list = new ArrayList<ItemVO>();

       	System.out.println(searchCategory);
       	System.out.println(searchWord);
       	
       	if(searchCategory == "title") {
       		list = service.titleSearch(searchWord);
       	}
       	else {
       		list = service.categorySearch(searchWord);
       	}
		
		model.addAttribute("list", list);
      
       	return "select";
       	
       }
    
    
   
}
