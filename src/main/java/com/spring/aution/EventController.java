package com.spring.aution;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.aution.service.EventService;
import com.spring.aution.vo.EventVO;

@Controller
public class EventController {
   private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
   
   
   @Autowired
   private EventService service;
   
   @RequestMapping(value = "/event", method = RequestMethod.GET)
	public String event(@RequestParam(value = "searchType", defaultValue = "none") String st,
			@RequestParam(value = "searchWord", defaultValue = "none") String sw, Model model) {
		logger.info("event(get) 메서드 실행.");

		 logger.info("검색 기준: {}", st); 
		 logger.info("검색어: {}", sw);
		 

		ArrayList<EventVO> list = service.search(st, sw);
		model.addAttribute("list", list);

		return "event";
	}
   
   @RequestMapping(value = "/getEvent", method = RequestMethod.GET)
	public String getEvent(@RequestParam("boardNum") int boardNum, Model model, HttpSession session) {
		logger.info("getEvent(get) 메서드 실행.");
		
		EventVO board = service.getEvent(boardNum);
		
		model.addAttribute("board", board);
		
		String userId = (String) session.getAttribute("userId");
		model.addAttribute("userId", userId);
		
		return "getEvent";
	}
   
	@RequestMapping(value = "/eventWrite", method = RequestMethod.GET)
	public String eventWrite() {
		return "eventWrite";
	}
	
	@RequestMapping(value = "/eventWrite", method = RequestMethod.POST)
	public String eventWrite(String boardTitle, String boardContent, HttpSession session, Model model) {
		logger.info("eventWrite(post) 메서드 실행.");

		logger.info("사용자가 입력한 글 제목: {}", boardTitle);
		logger.info("사용자가 입력한 글 내용: {}", boardContent);
		String boardId = (String) session.getAttribute("userId");
		logger.info("세션에서 가져온 아이디: {}", boardId);

		if (boardId == null) {
			logger.info("로그인을 하지 않은 상태.");
			
			return "redirect:/event";
		}

		boolean result = service.eventWrite(boardTitle, boardContent, boardId);

		String returnUrl = "";
		if (result) {
			logger.info("데이터 입력 성공.");
			returnUrl = "redirect:/event";
		} else {
			logger.info("데이터 입력 실패.");
			returnUrl = "eventWrite";
		}
		return returnUrl;
	}
	
	@RequestMapping(value = "/eventUpdate", method = RequestMethod.GET)
	public String eventUpdate(@RequestParam("boardNum")int boardNum, Model model) {
		logger.info("eventUpdate(get) 메서드 실행.");
		
		EventVO board = service.getEvent(boardNum);
		
		model.addAttribute("board", board);
		
		return "eventUpdate";
	}
	
	@RequestMapping(value = "/eventUpdate", method = RequestMethod.POST)
	public String eventUpdate(int boardNum, String boardTitle, String boardContent, HttpSession session) {
	
		logger.info("eventUpdate(post) 메서드 실행.");
		
		logger.info("사용자가 가진 글 번호: {}", boardNum);
		logger.info("사용자가 입력한 글 제목: {}", boardTitle);
		logger.info("사용자가 입력한 글 내용: {}", boardContent);
		
		// int boardNum = (int) session.getAttribute("boardNum");

		boolean result = service.eventUpdate(boardNum, boardTitle, boardContent);

		String returnUrl = "";
		if (result) {
			logger.info("데이터 입력 성공.");
			returnUrl = "redirect:/event";
		} else {
			logger.info("데이터 입력 실패.");
			returnUrl = "eventWrite";
		}
		return returnUrl;
		
	}
	
	@RequestMapping(value = "/eventDelete", method = RequestMethod.GET)
	public String eventDelete(int boardNum) {
		
		logger.info("eventDelete(get) 메서드 실행.");

		System.out.println(boardNum);
		service.eventDelete(boardNum);

		return "redirect:/event";
	}
	
	
   
	
   
   }