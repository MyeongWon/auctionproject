package com.spring.aution;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.aution.service.CommunityService;
import com.spring.aution.vo.CommunityVO;

@Controller
public class CommunityController {
   private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
   
   
   @Autowired
   private CommunityService service;
   
   @RequestMapping(value = "/community", method = RequestMethod.GET)
	public String community(@RequestParam(value = "searchType", defaultValue = "none") String st,
			@RequestParam(value = "searchWord", defaultValue = "none") String sw, Model model) {
		logger.info("community(get) 메서드 실행.");

		
		 logger.info("검색 기준: {}", st); 
		 logger.info("검색어: {}", sw);
		 

		ArrayList<CommunityVO> list = service.search(st, sw);
		model.addAttribute("list", list);

		return "community";
	}
   
   @RequestMapping(value = "/getCommunity", method = RequestMethod.GET)
	public String getCommunity(@RequestParam("boardNum") int boardNum, Model model, HttpSession session) {
		logger.info("getCommunity(get) 메서드 실행.");
		
		CommunityVO board = service.getCommunity(boardNum);
		model.addAttribute("board", board);
		
		String userId = (String) session.getAttribute("userId");
		model.addAttribute("userId", userId);
		
		return "getCommunity";
	}
   
	@RequestMapping(value = "/communityWrite", method = RequestMethod.GET)
	public String communityWrite() {
		return "communityWrite";
	}
	
	@RequestMapping(value = "/communityWrite", method = RequestMethod.POST)
	public String communityWrite(String boardTitle, String boardContent, HttpSession session, Model model) {
		logger.info("communityWrite(post) 메서드 실행.");

		logger.info("사용자가 입력한 글 제목: {}", boardTitle);
		logger.info("사용자가 입력한 글 내용: {}", boardContent);
		String boardId = (String) session.getAttribute("userId");
		logger.info("세션에서 가져온 아이디: {}", boardId);

		if (boardId == null) {
			logger.info("로그인을 하지 않은 상태.");
			
			return "redirect:/community";
		}

		boolean result = service.communityWrite(boardTitle, boardContent, boardId);

		String returnUrl = "";
		if (result) {
			logger.info("데이터 입력 성공.");
			returnUrl = "redirect:/community";
		} else {
			logger.info("데이터 입력 실패.");
			returnUrl = "communityWrite";
		}
		return returnUrl;
	}
	
	@RequestMapping(value = "/communityUpdate", method = RequestMethod.GET)
	public String communityUpdate(@RequestParam("boardNum") int boardNum, Model model) {
		logger.info("communityUpdate(get) 메서드 실행.");
		
		CommunityVO board = service.getCommunity(boardNum);
		
		model.addAttribute("board", board);
		
		return "communityUpdate";
	}
	
	@RequestMapping(value = "/communityUpdate", method = RequestMethod.POST)
	public String communityUpdate(int boardNum, String boardTitle, String boardContent, HttpSession session, Model model) {
	
		logger.info("communityUpdate(post) 메서드 실행.");
		
		logger.info("사용자가 가진 글 번호: {}", boardNum);
		logger.info("사용자가 입력한 글 제목: {}", boardTitle);
		logger.info("사용자가 입력한 글 내용: {}", boardContent);
		
		// int boardNum = (int) session.getAttribute("boardNum");

		boolean result = service.communityUpdate(boardNum, boardTitle, boardContent);

		String returnUrl = "";
		if (result) {
			logger.info("데이터 입력 성공.");
			returnUrl = "redirect:/community";
		} else {
			logger.info("데이터 입력 실패.");
			returnUrl = "communityUpdate";
		}
		
		return returnUrl;
		
	}
	
	@RequestMapping(value = "/communityDelete", method = RequestMethod.GET)
	public String communityDelete(int boardNum) {
		
		logger.info("communityDelete(get) 메서드 실행.");

		System.out.println(boardNum);
		service.communityDelete(boardNum);

		return "redirect:/community";
	}
	
	
	
   
   }