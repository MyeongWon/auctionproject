package com.spring.aution;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.aution.service.ItemService;
import com.spring.aution.service.UserService;
import com.spring.aution.vo.ItemVO;
import com.spring.aution.vo.UserVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private ItemService itemService;


	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		ArrayList<ItemVO> popularityList = itemService.popularityList(); 
		model.addAttribute("popularityList", popularityList);
		
		
		ArrayList<ItemVO> featuredList = itemService.featuredList(); 
		model.addAttribute("featuredList", featuredList);
		
		return "home";
	}
	
	@RequestMapping(value = "/getDigital", method = RequestMethod.GET)
	public String getDigital(Model model) {
		ArrayList<ItemVO> digitalList = itemService.digitalList(); 
		model.addAttribute("list", digitalList);
		
		System.out.println(digitalList);
		
		return "select";
	}
	
	
	@RequestMapping(value = "/getInterior", method = RequestMethod.GET)
	public String getInterior(Model model) {
		ArrayList<ItemVO> interiorList = itemService.interiorList(); 
		model.addAttribute("list", interiorList);
		
		System.out.println(interiorList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getInfant", method = RequestMethod.GET)
	public String getInfant(Model model) {
		ArrayList<ItemVO> infantList = itemService.infantList(); 
		model.addAttribute("list", infantList);
		
		System.out.println(infantList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getLife", method = RequestMethod.GET)
	public String getLife(Model model) {
		ArrayList<ItemVO> lifeList = itemService.lifeList(); 
		model.addAttribute("list", lifeList);
		
		System.out.println(lifeList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getSports", method = RequestMethod.GET)
	public String getSports(Model model) {
		ArrayList<ItemVO> sportsList = itemService.sportsList(); 
		model.addAttribute("list", sportsList);
		
		System.out.println(sportsList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getWoman", method = RequestMethod.GET)
	public String getWoman(Model model) {
		ArrayList<ItemVO> womanList = itemService.womanList(); 
		model.addAttribute("list", womanList);
		
		System.out.println(womanList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getMan", method = RequestMethod.GET)
	public String getMan(Model model) {
		ArrayList<ItemVO> manList = itemService.manList(); 
		model.addAttribute("list", manList);
		
		System.out.println(manList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getGame", method = RequestMethod.GET)
	public String getGame(Model model) {
		ArrayList<ItemVO> gameList = itemService.gameList(); 
		model.addAttribute("list", gameList);
		
		System.out.println(gameList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getBeauty", method = RequestMethod.GET)
	public String getBeauty(Model model) {
		ArrayList<ItemVO> beautyList = itemService.beautyList(); 
		model.addAttribute("list", beautyList);
		
		System.out.println(beautyList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getAnimal", method = RequestMethod.GET)
	public String getAnimal(Model model) {
		ArrayList<ItemVO> animalList = itemService.animalList(); 
		model.addAttribute("list", animalList);
		
		System.out.println(animalList);
		
		return "select";
	}
	
	@RequestMapping(value = "/getBook", method = RequestMethod.GET)
	public String getBook(Model model) {
		ArrayList<ItemVO> bookList = itemService.bookList(); 
		model.addAttribute("list", bookList);
		
		System.out.println(bookList);
		
		return "select";
	}



	
	//판매내역 나열
	@RequestMapping(value = "/sale", method = RequestMethod.GET)
	public String sale(HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("userId");
		ArrayList<ItemVO> list = itemService.getSaleList(sessionId); 
		model.addAttribute("list", list);
		return "sale";
	}
	//구매내역 나열
	@RequestMapping(value = "/purchase", method = RequestMethod.GET)
	public String purchase(Model model, HttpSession session) {
		String sessionId = (String) session.getAttribute("userId");
		ArrayList<ItemVO> list = itemService.getPurchaseList(sessionId); 
		model.addAttribute("list", list);
		return "purchase";
	}



	//찜목록 나열
	@RequestMapping(value = "/wishlist", method = RequestMethod.GET)
	public String wishlist(Model model, HttpSession session) {
		logger.info("wishlist get method 실행");

		String sessionId = (String) session.getAttribute("userId");
		ArrayList<ItemVO> list = itemService.getWishlistList(sessionId); 
		model.addAttribute("list", list);
		
		return "wishlist";
	}
	
	@RequestMapping(value = "/wishlistCheck", method = RequestMethod.GET)
	public String wishlistCheck(Model model, HttpSession session, int aucNum) {
		String sessionId = (String) session.getAttribute("userId");
		logger.info("wishlistCheck 메서드(get) 실행.");
		logger.info(sessionId);
		logger.info(Integer.toString(aucNum));
	      boolean result = itemService.wishlistCheck(sessionId,aucNum);
	      if (result) {
	         logger.info("찜 등록 성공.");
	      } else {
	         logger.info("찜 등록 실패.");
	      }
//		String strAucNum = Integer.toString(aucNum);
//	      model.addAttribute("aucNum", aucNum);
		return "forward:/oneSelect?aucNum=" + aucNum;
	}


	
	@RequestMapping(value = "/wishlistDelete", method = RequestMethod.GET)
	public String wishlistDelete(Model model, HttpSession session, int aucNum) {
		String sessionId = (String) session.getAttribute("userId");
		logger.info("wishlistDelete 메서드(get) 실행.");
		logger.info(sessionId);
		logger.info(Integer.toString(aucNum));
	      boolean result = itemService.wishlistDelete(sessionId,aucNum);
	      if (result) {
	         logger.info("찜 삭제 성공.");
	      } else {
	         logger.info("찜 삭제 실패.");
	      }
	      
		return "forward:/oneSelect?aucNum=" + aucNum;
	}

}
