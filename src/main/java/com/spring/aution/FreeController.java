package com.spring.aution;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.aution.service.FreeService;
import com.spring.aution.service.ReplyService;
import com.spring.aution.vo.FreeVO;
import com.spring.aution.vo.ReplyVO;

@Controller
public class FreeController {
   private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
   
   
   @Autowired
   private FreeService service;
   
   @Autowired
   private ReplyService replyService;
   
   
   @RequestMapping(value = "/free", method = RequestMethod.GET)
	public String free(@RequestParam(value = "searchType", defaultValue = "none") String st,
			@RequestParam(value = "searchWord", defaultValue = "none") String sw, Model model) {
		logger.info("free(get) 메서드 실행.");

		
		 logger.info("검색 기준: {}", st); 
		 logger.info("검색어: {}", sw);
		 

		ArrayList<FreeVO> list = service.search(st, sw);
		model.addAttribute("list", list);

		return "free";
	}
   
   
   @RequestMapping(value = "/getFree", method = RequestMethod.GET)
	public String getFree(@RequestParam("boardNum") int boardNum, Model model, HttpSession session) {
		logger.info("getFree(get) 메서드 실행.");
		
		FreeVO board = service.getFree(boardNum);
		
		model.addAttribute("board", board);
		
		 ArrayList<ReplyVO> replyList = replyService.getReply(boardNum);
		 ArrayList<String> writer = new ArrayList<String>();
		 
		 for(int i=0; i<replyList.size(); i++) {
			 writer.add(replyList.get(i).getWriter());	
		 }
		 	System.out.println(writer);
			model.addAttribute("writer", writer);
			
		
		String userId = (String) session.getAttribute("userId");
		model.addAttribute("userId", userId);
		
		session.setAttribute("boardNum", board.getBoardNum());

		return "getFree";
	}
   
	@RequestMapping(value = "/freeWrite", method = RequestMethod.GET)
	public String freeWrite() {
		return "freeWrite";
	}
	
	@RequestMapping(value = "/freeWrite", method = RequestMethod.POST)
	public String freeWrite(String boardTitle, String boardContent, HttpSession session, Model model) {
		logger.info("freeWrite(post) 메서드 실행.");

		logger.info("사용자가 입력한 글 제목: {}", boardTitle);
		logger.info("사용자가 입력한 글 내용: {}", boardContent);
		String boardId = (String) session.getAttribute("userId");
		logger.info("세션에서 가져온 아이디: {}", boardId);

		if (boardId == null) {
			logger.info("로그인을 하지 않은 상태.");
			
			return "redirect:/free";
		}

		boolean result = service.freeWrite(boardTitle, boardContent, boardId);

		String returnUrl = "";
		if (result) {
			logger.info("데이터 입력 성공.");
			returnUrl = "redirect:/free";
		} else {
			logger.info("데이터 입력 실패.");
			returnUrl = "freeWrite";
		}
		return returnUrl;
	}
	
	@RequestMapping(value = "/freeUpdate", method = RequestMethod.GET)
	public String freeUpdate(@RequestParam("boardNum")int boardNum, Model model) {
		logger.info("freeUpdate(get) 메서드 실행.");
		
		FreeVO board = service.getFree(boardNum);
		
		model.addAttribute("board", board);
		
		return "freeUpdate";
	}
	
	@RequestMapping(value = "/freeUpdate", method = RequestMethod.POST)
	public String freeUpdate(int boardNum, String boardTitle, String boardContent, HttpSession session) {
	
		logger.info("freeUpdate(post) 메서드 실행.");
		
		logger.info("사용자가 가진 글 번호: {}", boardNum);
		logger.info("사용자가 입력한 글 제목: {}", boardTitle);
		logger.info("사용자가 입력한 글 내용: {}", boardContent);
		
		// int boardNum = (int) session.getAttribute("boardNum");

		service.freeUpdate(boardNum, boardTitle, boardContent);

		
		
		return "redirect:/free";
	}
	
	@RequestMapping(value = "/freeDelete", method = RequestMethod.GET)
	public String freeDelete(int boardNum) {
		
		logger.info("freeDelete(get) 메서드 실행.");

		System.out.println(boardNum);
		service.freeDelete(boardNum);

		return "redirect:/free";
	}
	
	
	
   
   }