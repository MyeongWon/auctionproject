package com.spring.aution;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.aution.service.ReplyService;
import com.spring.aution.vo.ReplyVO;

@Controller
public class ReplyController {
   private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
   
   
   @Autowired
   private ReplyService service;
   
   
   @ResponseBody
	@RequestMapping(value = "/replyWrite", method = RequestMethod.POST)
	public void replyWrite(int bno, String content, HttpSession session, Model model) {
		 logger.info("replyWrite(post) 실행");
		 
		 String writer = (String) session.getAttribute("userId");
			
		 System.out.println(bno);
		 System.out.println(writer);
		 System.out.println(content);
		 
		 
		boolean result = service.replyWrite(bno, writer, content);

		if (result) {
			logger.info("댓글 입력 성공.");
		} else {
			logger.info("댓글 입력 실패.");
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/getReply", method = RequestMethod.GET)
	public ArrayList<ReplyVO> getReply(int boardNum){
		ArrayList<ReplyVO> result = new ArrayList<ReplyVO>();
		result = service.getReply(boardNum);
		
		System.out.println(boardNum);
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteReply", method = RequestMethod.POST)
	public void deleteReply(int rno){
		service.deleteReply(rno);
		System.out.println(rno);
	}
   }