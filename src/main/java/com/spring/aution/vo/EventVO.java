package com.spring.aution.vo;

import lombok.Data;

@Data
public class EventVO {

   private int boardNum;
   private String boardId;
   private String boardTitle;
   private String boardContent;
   private String boardDate;
}

