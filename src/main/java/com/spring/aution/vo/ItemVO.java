package com.spring.aution.vo;


import lombok.Data;

@Data
public class ItemVO {
	int aucNum;
	String aucCategory;
	String aucTitle;
	String aucDate;
	String aucLimit;
	int aucPriceStart;
	int aucPriceImmed;
	String aucSellId;
	String aucPurchaseId;
	String aucBidId;
	String aucContent;
	String aucImage;
	int aucStatus;
	int aucPopular;
}
