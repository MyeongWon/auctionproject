package com.spring.aution.vo;

import lombok.Data;

@Data
public class UserVO {

   private int userNum;
   private String userId;
   private String userEmail;
   private String userPassword;
   private String userNickname;
   private String userAddress;
   private String userPhonenum;
   private String userMembership;
   private int userPoint;
   private String userNh;
}
