package com.spring.aution.vo;

import lombok.Data;

@Data
public class ReplyVO {

   private int bno;
   private int rno;
   private String content;
   private String writer;
   private String regDate;

}

