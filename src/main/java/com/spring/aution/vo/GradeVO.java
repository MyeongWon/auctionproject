package com.spring.aution.vo;

import lombok.Data;

@Data
public class GradeVO {
	
   private int userGradeNum;
   private String userGradeId;
   private int userCheck;
   private int userGrade;
}
