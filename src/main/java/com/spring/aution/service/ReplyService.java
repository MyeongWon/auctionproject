package com.spring.aution.service;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.aution.dao.ReplyDAO;
import com.spring.aution.vo.ReplyVO;

@Service
public class ReplyService {
   
   @Autowired
   private ReplyDAO dao;

public boolean replyWrite(int bno, String writer, String content) {
	ReplyVO vo = new ReplyVO();
	vo.setBno(bno);
	vo.setWriter(writer);
	vo.setContent(content);
	
	int result = dao.replyWrite(vo);
	if (result > 0) 
		return true;
	return false;
}

public ArrayList<ReplyVO> getReply(int boardNum) {
	ArrayList<ReplyVO> result = dao.getReply(boardNum);

	return result;
}

public void deleteReply(int rno) {
	dao.deleteReply(rno);
	
}

}
