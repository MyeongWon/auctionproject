package com.spring.aution.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.aution.dao.ItemDAO;
import com.spring.aution.vo.ItemVO;

@Service
public class ItemService {

	@Autowired
	private ItemDAO dao;

	
	
	public boolean sell(ItemVO vo) {
		
		int result = dao.sell(vo);
		
		if(result > 0) {
			return true;
		} else {
			return false;
		}
	}
	public ArrayList<ItemVO> getList() {
		return dao.getList();
	}


	public ItemVO getOne(int aucNum) {
		return dao.getOne(aucNum);
	}
	
	
	public boolean pointCharge(int amount, String userId) {
	
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("amount", amount);
		map.put("userId",userId);
		
		int result = dao.pointCharge(map);
		
		if(result > 0) {
			return true;
		} else {
			return false;
		}
	}
	public ArrayList<ItemVO> getSaleList(String sessionId) {
		return dao.getSaleList(sessionId);
	}
	public ArrayList<ItemVO> getPurchaseList(String sessionId) {
		return dao.getPurchaseList(sessionId);
	}
	public void changeStatusSelled(String id, int aucNum) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("aucPurchaseId", id);
		map.put("aucNum", aucNum);
		dao.changeStatusSelled(map);
		
	}
	public void increasePopular(int aucNum) {
		dao.increasePopular(aucNum);
		
	}
	public ArrayList<ItemVO> search(String searchKeyword) {
		
		return dao.search(searchKeyword);
	}
	public ArrayList<ItemVO> popularityList() {
		
		return dao.popularityList();
	}
	public ArrayList<ItemVO> featuredList() {
		
		return dao.featuredList();
	}
	public ArrayList<ItemVO> digitalList() {
		
		return dao.digitalList();
	}
	public ArrayList<ItemVO> interiorList() {
		
		return dao.interiorList();
	}
	
	public ArrayList<ItemVO> infantList() {

		return dao.infantList();
	}
	public ArrayList<ItemVO> lifeList() {

		return dao.lifeList();
	}
	public ArrayList<ItemVO> sportsList() {

		return dao.sportsList();
	}
	public ArrayList<ItemVO> womanList() {

		return dao.womanList();
	}
	public ArrayList<ItemVO> manList() {

		return dao.manList();
	}
	public ArrayList<ItemVO> gameList() {

		return dao.gameList();
	}
	public ArrayList<ItemVO> beautyList() {

		return dao.beautyList();
	}
	public ArrayList<ItemVO> animalList() {

		return dao.animalList();
	}
	public ArrayList<ItemVO> bookList() {

		return dao.bookList();
	}
	
	public boolean setAuctionStatus(int aucNum, int priceStart, String userId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("price", priceStart);
		map.put("aucNum", aucNum);

		
		int result = dao.setAuctionStatus(map);
		
		if(result >= 0) {
			return true;
		} else {
			return false;
		}
	}
	public boolean setNewBuyerPoint(String userId, int priceStart) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("price", priceStart);
		
		
		int result = dao.setNewBuyerPoint(map);
		
		if(result >= 0) {
			return true;
		} else {
			return false;
		}
	}
	public boolean setFormerBuyerPoint(String bidId, int nowPrice) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userId", bidId);
		map.put("price", nowPrice);
		
		
		int result = dao.setFormerBuyerPoint(map);
		
		if(result >= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<ItemVO> getWishlistList(String sessionId) {
		return dao.getWishlistList(sessionId);
	}
	
	public boolean wishlistCheck(String sessionId, int aucNum) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sessionId", sessionId);
		map.put("aucNum", aucNum);
		
		int result = dao.wishlistCheck(map);
	      
	    if (result > 0)
	    	return true;
	    else
	        return false;
	}
	public boolean wishlistDelete(String sessionId, int aucNum) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sessionId", sessionId);
		map.put("aucNum", aucNum);
		
		int result = dao.wishlistDelete(map);
	      
	    if (result > 0)
	    	return true;
	    else
	        return false;
	}
	public boolean getWishlistNum(String sessionId, int aucNum) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sessionId", sessionId);
		map.put("aucNum", aucNum);
		String resultstr = dao.getWishlistNum(map);
		
		if(resultstr == null) {
			return true; //찜 테이블에 찜이 없음
		}
		else{
			return false; //있음

		}
	}
	public boolean setBidComplete(int aucNum, String bidId) {
		ItemVO vo = new ItemVO();
		vo.setAucNum(aucNum);
		vo.setAucBidId(bidId);
	
		
		int result = dao.setBidComplete(vo);
	      
	    if (result > 0)
	    	return true;
	    else
	        return false;

	}
	public ArrayList<ItemVO> categorySearch(String searchKeyword) {
		return dao.categorySearch(searchKeyword);
	}
	public ArrayList<ItemVO> titleSearch(String searchWord) {
		System.out.println("service:"+searchWord);
		return dao.titleSearch(searchWord);
	}
	public String getSellId(int aucNum) {
		String result = dao.getSellId(aucNum);
		return result;
	}
	public void updateStatus(int aucNum) {
		dao.updateStatus(aucNum);
		
	}


}
	
	

