package com.spring.aution.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.aution.dao.EventDAO;
import com.spring.aution.vo.EventVO;

@Service
public class EventService {
   
   @Autowired
   private EventDAO dao;

public ArrayList<EventVO> search(String st, String sw) {
	HashMap<String, Object> map = new HashMap<String, Object>();
	map.put("searchType", st);
	map.put("searchWord", sw);
	
	return dao.search(map);
}

public boolean eventWrite(String boardTitle, String boardContent, String boardId) {
	EventVO vo = new EventVO();
	vo.setBoardTitle(boardTitle);
	vo.setBoardContent(boardContent);
	vo.setBoardId(boardId);
	
	int result = dao.eventWrite(vo);
	if (result > 0)
		return true;
	return false;
}


public EventVO getEvent(int boardNum) {
	return dao.getEvent(boardNum);
}

public ArrayList<EventVO> getList() {
	return dao.getList();
}


public void eventDelete(int boardNum) {
	
	dao.eventDelete(boardNum);
	
}

public boolean eventUpdate(int boardNum, String boardTitle, String boardContent) {
	EventVO vo = new EventVO();
	vo.setBoardNum(boardNum);
	vo.setBoardTitle(boardTitle);
	vo.setBoardContent(boardContent);
	
	int result = dao.eventUpdate(vo);
	if (result > 0)
		return true;
	return false;
}

}
