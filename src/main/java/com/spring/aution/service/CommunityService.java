package com.spring.aution.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.aution.dao.CommunityDAO;
import com.spring.aution.vo.CommunityVO;

@Service
public class CommunityService {
   
   @Autowired
   private CommunityDAO dao;

public ArrayList<CommunityVO> search(String st, String sw) {
	HashMap<String, Object> map = new HashMap<String, Object>();
	map.put("searchType", st);
	map.put("searchWord", sw);
	
	return dao.search(map);
}

public boolean communityWrite(String boardTitle, String boardContent, String boardId) {
	CommunityVO vo = new CommunityVO();
	vo.setBoardTitle(boardTitle);
	vo.setBoardContent(boardContent);
	vo.setBoardId(boardId);
	
	int result = dao.communityWrite(vo);
	if (result > 0)
		return true;
	return false;
}


public CommunityVO getCommunity(int boardNum) {
	return dao.getCommunity(boardNum);
}

public ArrayList<CommunityVO> getList() {
	return dao.getList();
}


public void communityDelete(int boardNum) {
	
	dao.communityDelete(boardNum);
	
}

public boolean communityUpdate(int boardNum, String boardTitle, String boardContent) {
	CommunityVO vo = new CommunityVO();
	vo.setBoardNum(boardNum);
	vo.setBoardTitle(boardTitle);
	vo.setBoardContent(boardContent);
	
	int result = dao.communityUpdate(vo);
	if (result > 0)
		return true;
	return false;
}




}
