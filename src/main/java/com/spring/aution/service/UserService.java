package com.spring.aution.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.aution.dao.UserDAO;
import com.spring.aution.vo.GradeVO;
import com.spring.aution.vo.ItemVO;
import com.spring.aution.vo.UserVO;

@Service
public class UserService {
   
   @Autowired
   private UserDAO dao;
   

   public UserVO login(String userId, String userPassword) {
      UserVO loginUser = new UserVO();
      loginUser.setUserId(userId);
      loginUser.setUserPassword(userPassword);
      
      UserVO result = dao.login(loginUser);
      
      return result;
   }

   public boolean join(String userId, String userEmail, String userPassword, String userNickname
                  , String postcode, String roadAddress, String detailAddress, String extraAddress, String userPhonenum, String userMembership) {
      UserVO newUser = new UserVO();
      newUser.setUserId(userId);
      newUser.setUserEmail(userEmail);
      newUser.setUserPassword(userPassword);
      newUser.setUserNickname(userNickname);
      
      String userAddress = postcode.concat(roadAddress.concat(detailAddress.concat(extraAddress)));
      
      newUser.setUserAddress(userAddress);
      newUser.setUserPhonenum(userPhonenum);
      newUser.setUserMembership(userMembership);
      
      
      int result = dao.join(newUser);
      
      if (result > 0)
         return true;
      else
         return false;
   }




   public boolean idCheck(String userId) {
		String result = dao.idCheck(userId);
		
		if (result == null) {
			// 중복되지 않은 ID = 사용할 수 있는 ID
			return false;
		} else {
			// 중복된 ID
			return true;
		}
	}

   public UserVO getUser(String userId) {
	   
	return dao.getUser(userId);
	
}

   public boolean subtractPoint(String id, int priceImmed) {
	   UserVO confirmUser = getUser(id);
	   
	   int point = confirmUser.getUserPoint();
	   int subtract = point-priceImmed;
	   if(subtract > 0) {
		   confirmUser.setUserPoint(subtract);
		   dao.subtractPoint(confirmUser);
		   return true;
	   }
	   else {
		   return false;
	   }
	
}

public boolean nhSetting(String userId, String centerAddr) {
	UserVO settingUser = new UserVO();
	settingUser.setUserId(userId);
	settingUser.setUserNh(centerAddr);

	int result = dao.nhSetting(settingUser);

	if (result > 0)
		return true;
	else
		return false;
}

public boolean increasePoint(String sellerId, int priceImmed) {
	HashMap<String, Object> map = new HashMap<String, Object>();
	map.put("userId", sellerId);
	map.put("priceImmed", priceImmed);
	
	int result = dao.increasePoint(map);

	if (result > 0)
		
		return true;
	else
		
		return false;
}


public boolean mnInformation(String userId, String userEmail, String userPassword, String userNickname
        , String postcode, String roadAddress, String detailAddress, String extraAddress, String userPhonenum) {
UserVO updateUser = new UserVO();
updateUser.setUserId(userId);
updateUser.setUserEmail(userEmail);
updateUser.setUserPassword(userPassword);
updateUser.setUserNickname(userNickname);

String userAddress = postcode.concat(roadAddress.concat(detailAddress.concat(extraAddress)));

updateUser.setUserAddress(userAddress);
updateUser.setUserPhonenum(userPhonenum);


int result = dao.mnInformation(updateUser);

if (result > 0)
   return true;
else
   return false;
}

public void userDelete(String userId) {
	
	dao.userDelete(userId);
}

public int passChk(String userId, String userPassword) {
	
	UserVO passChkUser = new UserVO();
	passChkUser.setUserId(userId);
	passChkUser.setUserPassword(userPassword);
	
	int result = dao.passChk(passChkUser);
	
	return result;
}

public boolean socialIdCheck(String kakaoEmail, String kakaoNickname) {
	HashMap<String, Object> map = new HashMap<String, Object>();

	map.put("kakaoEmail", kakaoEmail);
	map.put("kakaoNickname", kakaoNickname);
	
	String result = dao.socialIdCheck(map);
	
	if (result == null) {
	//디비에 아이디가 없음
		return false;
	} else {
	//디비에 아이디가 있음
		return true;
	}
}

public void insertGrade(String aucSellId, String ratingString) {
	   int rating = Integer.parseInt(ratingString);
	   
	   GradeVO insertGrade = new GradeVO();
	   insertGrade.setUserGrade(rating);
	   insertGrade.setUserGradeId(aucSellId);
	   
	   dao.insertGrade(insertGrade);
	   
}

public ArrayList<GradeVO> getGradeList(String id) {
	System.out.println(dao.getGradeList(id));
	return dao.getGradeList(id);
}







}






   


