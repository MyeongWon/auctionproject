package com.spring.aution.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.aution.dao.FreeDAO;
import com.spring.aution.vo.FreeVO;

@Service
public class FreeService {
   
   @Autowired
   private FreeDAO dao;

public ArrayList<FreeVO> search(String st, String sw) {
	HashMap<String, Object> map = new HashMap<String, Object>();
	map.put("searchType", st);
	map.put("searchWord", sw);
	
	return dao.search(map);
}

public boolean freeWrite(String boardTitle, String boardContent, String boardId) {
	FreeVO vo = new FreeVO();
	vo.setBoardTitle(boardTitle);
	vo.setBoardContent(boardContent);
	vo.setBoardId(boardId);
	
	int result = dao.freeWrite(vo);
	if (result > 0)
		return true;
	return false;
}


public FreeVO getFree(int boardNum) {
	return dao.getFree(boardNum);
}

public ArrayList<FreeVO> getList() {
	return dao.getList();
}


public void freeDelete(int boardNum) {
	
	dao.freeDelete(boardNum);
	
}

public boolean freeUpdate(int boardNum, String boardTitle, String boardContent) {
	FreeVO vo = new FreeVO();
	vo.setBoardNum(boardNum);
	vo.setBoardTitle(boardTitle);
	vo.setBoardContent(boardContent);
	
	int result = dao.freeUpdate(vo);
	if (result > 0)
		return true;
	return false;
}





}
