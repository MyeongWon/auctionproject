package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.aution.vo.FreeVO;

@Repository
public class FreeDAO {
	
	@Autowired
	private SqlSession session;

	public int freeWrite(FreeVO vo) {
		int result = 0;
		
		try {
			FreeMapper mapper = session.getMapper(FreeMapper.class);
			result = mapper.freeWrite(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<FreeVO> search(HashMap<String, Object> map) {
		ArrayList<FreeVO> result = null;
		
		try {
			FreeMapper mapper = session.getMapper(FreeMapper.class);
			result = mapper.search(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}


	public FreeVO getFree(int boardNum) {
		FreeVO result = null;
		
		try {
			FreeMapper mapper = session.getMapper(FreeMapper.class);
			result = mapper.getFree(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<FreeVO> getList() {
		ArrayList<FreeVO> result = null;
		
		try {
			FreeMapper mapper = session.getMapper(FreeMapper.class);
			result = mapper.getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}


	public void freeDelete(int boardNum) {
		
		try {
			FreeMapper mapper = session.getMapper(FreeMapper.class);
			mapper.freeDelete(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public int freeUpdate(FreeVO vo) {
		int result = 0;
		
		try {
			FreeMapper mapper = session.getMapper(FreeMapper.class);
			result = mapper.freeUpdate(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}



}
