package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.aution.vo.CommunityVO;

@Repository
public class CommunityDAO {
	
	@Autowired
	private SqlSession session;

	public int communityWrite(CommunityVO vo) {
		int result = 0;
		
		try {
			CommunityMapper mapper = session.getMapper(CommunityMapper.class);
			result = mapper.communityWrite(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<CommunityVO> search(HashMap<String, Object> map) {
		ArrayList<CommunityVO> result = null;
		
		try {
			CommunityMapper mapper = session.getMapper(CommunityMapper.class);
			result = mapper.search(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}


	public CommunityVO getCommunity(int boardNum) {
		CommunityVO result = null;
		
		try {
			CommunityMapper mapper = session.getMapper(CommunityMapper.class);
			result = mapper.getCommunity(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<CommunityVO> getList() {
		ArrayList<CommunityVO> result = null;
		
		try {
			CommunityMapper mapper = session.getMapper(CommunityMapper.class);
			result = mapper.getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}


	public void communityDelete(int boardNum) {
		
		try {
			CommunityMapper mapper = session.getMapper(CommunityMapper.class);
			mapper.communityDelete(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public int communityUpdate(CommunityVO vo) {
		int result = 0;
		
		try {
			CommunityMapper mapper = session.getMapper(CommunityMapper.class);
			result = mapper.communityUpdate(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	
	
	
	


}
