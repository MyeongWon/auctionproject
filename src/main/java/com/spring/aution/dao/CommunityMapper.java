package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.spring.aution.vo.CommunityVO;

public interface CommunityMapper {

	ArrayList<CommunityVO> search(HashMap<String, Object> map);

	int communityWrite(CommunityVO vo);

	CommunityVO getCommunity(int boardNum);

	ArrayList<CommunityVO> getList();

	void communityDelete(int boardNum);

	int communityUpdate(CommunityVO vo);
	
}
