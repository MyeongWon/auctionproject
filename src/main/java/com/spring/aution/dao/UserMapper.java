package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.spring.aution.vo.GradeVO;
import com.spring.aution.vo.ItemVO;
import com.spring.aution.vo.UserVO;

public interface UserMapper {

	public int join(UserVO newUser);

	public UserVO login(UserVO loginUser);

	public String idCheck(String userId);

	public UserVO getUser(String userId);

	public int subtractPoint(UserVO user);

	public int nhSetting(UserVO settingUser);

	public int increasePoint(HashMap<String, Object> map);
	
	public int passChk(UserVO passChkUser);

	public int mnInformation(UserVO updateUser);

	public void userDelete(String userId);

	public String socialIdCheck(HashMap<String, Object> map);

	public void insertGrade(GradeVO insertGrade);

	public ArrayList<GradeVO> getGradeList(String id);

}
