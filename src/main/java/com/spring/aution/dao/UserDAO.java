package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.aution.vo.GradeVO;
import com.spring.aution.vo.UserVO;

@Repository
public class UserDAO {
   
   @Autowired
   private SqlSession session;
   
   public int join(UserVO newUser) {
      int result = 0;
      
      try {
         UserMapper mapper = session.getMapper(UserMapper.class);
         result = mapper.join(newUser);
      } catch (Exception e) {
         e.printStackTrace();
      }
      
      return result;
   }

   

   public UserVO login(UserVO loginUser) {
      UserVO result = null;
      
      try {
         UserMapper mapper = session.getMapper(UserMapper.class);
         result = mapper.login(loginUser);
      } catch (Exception e) {
         e.printStackTrace();
      }
      
      return result;
   }




   public String idCheck(String userId) {
		String result = null;
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			result = mapper.idCheck(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}




	public UserVO getUser(String userId) {
		UserVO result = null;

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			result = mapper.getUser(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public int subtractPoint(UserVO user) {
		int result = 0;

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			result = mapper.subtractPoint(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}




	public int nhSetting(UserVO settingUser) {
		
		int result = 0;

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);			
			result = mapper.nhSetting(settingUser);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
		
	}

	public int passChk(UserVO passChkUser) {

		int result = 0;

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);			
			result = mapper.passChk(passChkUser);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}





	public int increasePoint(HashMap<String, Object> map) {
		int result = 0;

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			result = mapper.increasePoint(map);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}


	public int mnInformation(UserVO updateUser) {
		int result = 0;
	      
	      try {
	         UserMapper mapper = session.getMapper(UserMapper.class);
	         result = mapper.mnInformation(updateUser);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	      
	      return result;
	}



	public void userDelete(String userId) {
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
	        mapper.userDelete(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	public String socialIdCheck(HashMap<String, Object> map) {
		String result = null;
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			result = mapper.socialIdCheck(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public void insertGrade(GradeVO insertGrade) {
	      try {
	         UserMapper mapper = session.getMapper(UserMapper.class);
	         mapper.insertGrade(insertGrade);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }



	public ArrayList<GradeVO> getGradeList(String id) {
		ArrayList<GradeVO> result = null;
		System.out.println(id);
		try {
	         UserMapper mapper = session.getMapper(UserMapper.class);
	         result = mapper.getGradeList(id);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
		
		return result;
	}



	

	

   

}
