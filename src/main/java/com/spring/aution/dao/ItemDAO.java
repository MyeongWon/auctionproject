package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.aution.vo.ItemVO;

@Repository
public class ItemDAO {

	@Autowired
	private SqlSession session;
	
	public ArrayList<ItemVO> getList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.getList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}
	
	public int sell(ItemVO vo) {
		int result = 0;
		
		try {
			ItemMapper mapper= session.getMapper(ItemMapper.class);
			result = mapper.sell(vo);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result; 
	}


	public ItemVO getOne(int aucNum) {
		ItemVO result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.getOne(aucNum);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}



	public int pointCharge(HashMap<String, Object> map) {
		int result = 0;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.pointCharge(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 

	
		return result;
	}

	public ArrayList<ItemVO> getSaleList(String sessionId) {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.getSaleList(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}

	public ArrayList<ItemVO> getPurchaseList(String sessionId) {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.getPurchaseList(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}

	public void changeStatusSelled(HashMap<String, Object> map) {
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			mapper.changeStatusSelled(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}

	public void increasePopular(int aucNum) {
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			mapper.increasePopular(aucNum);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}


	public int setAuctionStatus(HashMap<String, Object> map) {
		int result = 0;
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.setAuctionStatus(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 

	
		return result;
	}

	public int setNewBuyerPoint(HashMap<String, Object> map) {
		int result = 0;
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.setNewBuyerPoint(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 

	
		return result;
	}

	public int setFormerBuyerPoint(HashMap<String, Object> map) {
		int result = 0;
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.setFormerBuyerPoint(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}

	

	public ArrayList<ItemVO> getWishlistList(String sessionId) {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.getWishlistList(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}
	
	public ArrayList<ItemVO> search(String searchKeyword) {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.search(searchKeyword);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> popularityList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.popularityList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> featuredList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.featuredList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> digitalList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.digitalList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> interiorList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.interiorList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}
	

	public ArrayList<ItemVO> infantList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.infantList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> lifeList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.lifeList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> sportsList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.sportsList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> womanList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.womanList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> manList() {

		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.manList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}


	public int wishlistCheck(HashMap<String, Object> map) {
		int result = 0;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.wishlistCheck(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
		}

	public int wishlistDelete(HashMap<String, Object> map) {
		int result = 0;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.wishlistDelete(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}

	public String getWishlistNum(HashMap<String, Object> map) {
		String result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.getWishlistNum(map);
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return result;
	}



	public ArrayList<ItemVO> gameList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.gameList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> beautyList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.beautyList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> animalList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.animalList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> bookList() {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.bookList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public int setBidComplete(ItemVO vo) {
		int result = 0;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.setBidComplete(vo);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}

	public ArrayList<ItemVO> categorySearch(String searchKeyword) {
		ArrayList<ItemVO> result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.categorySearch(searchKeyword);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public ArrayList<ItemVO> titleSearch(String searchWord) {
		ArrayList<ItemVO> result = null;
		System.out.println("dao:"+searchWord);
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.titleSearch(searchWord);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public String getSellId(int aucNum) {
		String result = null;
		
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			result = mapper.getSellId(aucNum);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public void updateStatus(int aucNum) {
		try {
			ItemMapper mapper = session.getMapper(ItemMapper.class);
			mapper.updateStatus(aucNum);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}


}
