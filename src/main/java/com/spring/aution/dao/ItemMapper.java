package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.spring.aution.vo.ItemVO;

public interface ItemMapper {

	int sell(ItemVO vo);

	ItemVO getOne(int aucNum);

	ArrayList<ItemVO> getList();
	
	int getUser(HashMap<String, Object> map);
	
	int pointCharge(HashMap<String, Object> map);

	ArrayList<ItemVO> getSaleList(String sessionId);

	ArrayList<ItemVO> getPurchaseList(String sessionId);

	void changeStatusSelled(HashMap<String, Object> map);

	void increasePopular(int aucNum);

	ArrayList<ItemVO> search(String searchKeyword);

	ArrayList<ItemVO> popularityList();

	ArrayList<ItemVO> featuredList();

	ArrayList<ItemVO> digitalList();

	ArrayList<ItemVO> interiorList();

	ArrayList<ItemVO> infantList();

	ArrayList<ItemVO> lifeList();

	ArrayList<ItemVO> sportsList();

	ArrayList<ItemVO> womanList();

	ArrayList<ItemVO> manList();

	ArrayList<ItemVO> gameList();

	ArrayList<ItemVO> beautyList();

	ArrayList<ItemVO> animalList();

	ArrayList<ItemVO> bookList();



	int setAuctionStatus(HashMap<String, Object> map);

	int setNewBuyerPoint(HashMap<String, Object> map);

	int setFormerBuyerPoint(HashMap<String, Object> map);

	ArrayList<ItemVO> getWishlistList(String sessionId);

	int wishlistCheck(HashMap<String, Object> map);

	String getWishlistNum(HashMap<String, Object> map);

	int wishlistDelete(HashMap<String, Object> map);

	int setBidComplete(ItemVO vo);

	ArrayList<ItemVO> categorySearch(String searchKeyword);

	ArrayList<ItemVO> titleSearch(String searchWord);

	String getSellId(int aucNum);

	void updateStatus(int aucNum);






}
