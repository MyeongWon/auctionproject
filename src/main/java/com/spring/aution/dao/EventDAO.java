package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.aution.vo.EventVO;

@Repository
public class EventDAO {
	
	@Autowired
	private SqlSession session;
	
	public ArrayList<EventVO> search(HashMap<String, Object> map) {
		ArrayList<EventVO> result = null;
		
		try {
			EventMapper mapper = session.getMapper(EventMapper.class);
			result = mapper.search(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	
	public int eventWrite(EventVO vo) {
		int result = 0;
		
		try {
			EventMapper mapper = session.getMapper(EventMapper.class);
			result = mapper.eventWrite(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}



	public EventVO getEvent(int boardNum) {
		EventVO result = null;
		
		try {
			EventMapper mapper = session.getMapper(EventMapper.class);
			result = mapper.getEvent(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<EventVO> getList() {
		ArrayList<EventVO> result = null;
		
		try {
			EventMapper mapper = session.getMapper(EventMapper.class);
			result = mapper.getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}


	public void eventDelete(int boardNum) {
		
		try {
			EventMapper mapper = session.getMapper(EventMapper.class);
			mapper.eventDelete(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public int eventUpdate(EventVO vo) {
		int result = 0;
		
		try {
			EventMapper mapper = session.getMapper(EventMapper.class);
			result = mapper.eventUpdate(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

}
