package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.spring.aution.vo.FreeVO;

public interface FreeMapper {

	ArrayList<FreeVO> search(HashMap<String, Object> map);

	int freeWrite(FreeVO vo);

	FreeVO getFree(int boardNum);

	ArrayList<FreeVO> getList();

	void freeDelete(int boardNum);

	int freeUpdate(FreeVO vo);

}
