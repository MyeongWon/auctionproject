package com.spring.aution.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.spring.aution.vo.EventVO;

public interface EventMapper {

	ArrayList<EventVO> search(HashMap<String, Object> map);

	int eventWrite(EventVO vo);

	EventVO getEvent(int boardNum);

	ArrayList<EventVO> getList();

	void eventDelete(int boardNum);

	int eventUpdate(EventVO vo);

}
