package com.spring.aution.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.aution.vo.ReplyVO;

@Repository
public class ReplyDAO {
	
	@Autowired
	private SqlSession session;

	public int replyWrite(ReplyVO vo) {
		int result = 0;
		
		try {
			ReplyMapper mapper = session.getMapper(ReplyMapper.class);
			result = mapper.replyWrite(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<ReplyVO> getReply(int boardNum) {
		ArrayList<ReplyVO> result = null;
		
		try {
			ReplyMapper mapper = session.getMapper(ReplyMapper.class);
			result = mapper.getReply(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public void deleteReply(int rno) {
		try {
			ReplyMapper mapper = session.getMapper(ReplyMapper.class);
			mapper.deleteReply(rno);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	
	


}
