package com.spring.aution.dao;

import java.util.ArrayList;

import com.spring.aution.vo.ReplyVO;

public interface ReplyMapper {

	int replyWrite(ReplyVO vo);

	ArrayList<ReplyVO> getReply(int boardNum);

	void deleteReply(int rno);

}
