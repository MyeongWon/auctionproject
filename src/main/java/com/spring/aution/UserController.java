package com.spring.aution;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.aution.service.ItemService;
import com.spring.aution.service.UserService;
import com.spring.aution.vo.GradeVO;
import com.spring.aution.vo.ItemVO;
import com.spring.aution.vo.UserVO;

@Controller
public class UserController {
   private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
   
   @Autowired
   private UserService service;
   
   @Autowired
   private ItemService itemService;
   
   @RequestMapping(value = "/login", method = RequestMethod.GET)
   public String login() {
      return "login";
   }
   @RequestMapping(value = "/join", method = RequestMethod.GET)
   public String join() {
      return "join";
   }
   
   
   @RequestMapping(value = "/login", method = RequestMethod.POST)
   public String login(String userId, String userPassword, Model model, HttpSession session) {
      logger.info("login(post) 메서드 실행.");
      
   
      UserVO result = service.login(userId, userPassword); //닉네임이나 널이 나옴
      
      if (result != null) {
         logger.info("로그인 성공");
         session.setAttribute("userId", result.getUserId());
         session.setAttribute("userNickname", result.getUserNickname()); 
         System.out.println(result.getUserNum());
         
         int userPoint = result.getUserPoint();
         session.setAttribute("userPoint", userPoint);         
         return "redirect:/";
      } else {
         logger.info("로그인 실패");
         model.addAttribute("errMsg", "로그인에 실패했습니다. ID 또는 비밀번호를 확인해주세요.");
         return "redirect:/login";
      }
      
   }
    
   @RequestMapping(value = "/join", method = RequestMethod.POST)
   public String join(String userId, String userEmail, String userPassword, String userNickname
		   , String postcode, String roadAddress, String detailAddress, String extraAddress, String userPhonenum, String userMembership) {
      logger.info("join 메서드(post) 실행.");
      boolean result = service.join(userId, userEmail, userPassword, userNickname
    		  , postcode, roadAddress, detailAddress, extraAddress, userPhonenum, userMembership);
      
      if (result) {
         logger.info("회원가입 성공.");
      } else {
         logger.info("회원가입 실패.");
      }
      
      return "home";
   }
   
   @RequestMapping(value = "/logout", method = RequestMethod.GET)
   public String logout(HttpSession session) {
      logger.info("logout(get) 메서드 실행.");
      
      //session.removeAttribute("userNickname");
      session.invalidate();
      
      return "redirect:/";
   }
   @RequestMapping(value = "/idCheck", method = RequestMethod.GET)
	public String idCheck(Model model, String userId) {
		logger.info("idCheck(get) 메서드 실행.");
		
		// 출처: 회원가입 페이지에서 사용자가 중복검사를 실행하고자 입력한 userId
		// 의미: 사용자가 입력한 데이터를 새창에서 표시해주기 위함.
		model.addAttribute("userId", userId);
		
		return "idCheck";
	}
	
	@RequestMapping(value = "/idCheck", method = RequestMethod.POST)
	public String idCheck(String userId, Model model) {
		logger.info("idCheck(post) 메서드 실행.");
		
		boolean result = service.idCheck(userId);
		String resultStr = "";
		
		if (result) {
			logger.info("중복된 ID");
			resultStr = "중복된 ID입니다.";
		} else {
			logger.info("사용할 수 있는 ID");
			resultStr = "사용할 수 있는 ID입니다.";
			
			// 출처: 중복검사 페이지에서 사용자가 검색 버튼 누를 때 입력한 userId
			// 의미: 중복검사 결과 사용할 수 있는 것으로 판명난 userId
			model.addAttribute("userId", userId);
		}
		model.addAttribute("result", resultStr);
		
		return "idCheck";
	}
	
	@RequestMapping(value = "/myPage", method = RequestMethod.GET)
	public String myPage(Model model, HttpSession session) {
		
		String id = (String)session.getAttribute("userId");
		
		logger.info(id);
		UserVO user = service.getUser(id);
		
		//동네 설정 문자열 가공
//		String spliter = user.getUserNh();
//		String[] splitArray = spliter.split(" ");
//		String resultStr = splitArray[1] + " " + splitArray[2];
//		user.setUserNh(resultStr);
//		
		model.addAttribute("user",user);
		ArrayList<ItemVO> saleList = itemService.getSaleList(id); 
		model.addAttribute("saleList", saleList);
		ArrayList<ItemVO> purchaseList = itemService.getPurchaseList(id); 
		model.addAttribute("purchaseList", purchaseList);
		System.out.println(saleList);
		System.out.println(purchaseList);
		
		//별점을 띄워보자
		ArrayList<GradeVO> gradeList = service.getGradeList(id);
		int userGrade = 0;
		if(gradeList==null) {
			System.out.println("나와라");
		} else {
		
		for(int i=0; i<gradeList.size(); i++) {
			userGrade += gradeList.get(i).getUserGrade();
		}
		model.addAttribute("userGrade", userGrade);
		
		int userCheck = 0;
		for(int i=0; i<gradeList.size(); i++) {
			userCheck += gradeList.get(i).getUserCheck();
		}
		model.addAttribute("userCheck", userCheck);
		
		System.out.println(userGrade);
		System.out.println(userCheck);
		}
		
		model.addAttribute("gradeList", gradeList);
		
		return "myPage";
		
	}
	
	@RequestMapping(value = "/nhSetting", method = RequestMethod.GET)
	public String nhSetting() {
		return "nhSetting";
	}
	
	@RequestMapping(value = "/nhSetting", method = RequestMethod.POST)
	public String nhSetting(String centerAddr, HttpSession session) {
		String userId = (String) session.getAttribute("userId");
		
		boolean result = service.nhSetting(userId, centerAddr);
		
		if (result) {
			logger.info("데이터 입력 성공.");
		} else {
			logger.info("데이터 입력 실패.");
		}
		
		return "redirect:/myPage";
	}

	
	@RequestMapping(value="/passChk", method = RequestMethod.GET)
	public String passChk(Model model, HttpSession session) {
		logger.info("passChk 메서드(get) 실행.");
		
		String userId = (String) session.getAttribute("userId");
		model.addAttribute("userId", userId);
		
		
		UserVO user = service.getUser(userId);
		model.addAttribute("user", user);
		
		
		
		return "passChk";
	}
	
	@RequestMapping(value="/passChk", method = RequestMethod.POST)
	public String passChk(String userPassword, Model model, HttpSession session) {
		logger.info("passChk 메서드(post) 실행.");
		
		String userId = (String) session.getAttribute("userId");
		model.addAttribute("userId", userId);
		
		int result  = service.passChk(userId, userPassword);
		
		
		String returnUrl = "";
		
		if (result == 0) {
			model.addAttribute("msg", "비밀번호가 틀립니다.");	
			returnUrl = "redirect:/passChk";
		} else if (result == 1) {
			model.addAttribute("msg", "비밀번호가 맞습니다.");
			returnUrl = "mnInformation";
		} else {
			model.addAttribute("msg", "문의 바랍니다.");
			returnUrl = "redirect:/passChk";  
		}
		
		
		return returnUrl;
	}
	
	@RequestMapping(value = "/userDelete", method = RequestMethod.GET)
	public String userDelete(String userId, HttpSession session) {
		
		logger.info("userDelete(get) 메서드 실행.");

		System.out.println(userId);
		service.userDelete(userId);
		
		session.invalidate();

		return "home";
	}
	
	
	

	@RequestMapping(value = "/mnInformation", method = RequestMethod.GET)
	public String mnInformation(Model model, HttpSession session) {
		logger.info("mnInformation 메서드(get) 실행.");
		

		String userId = (String) session.getAttribute("userId");
		System.out.println(userId);
		
		UserVO user = service.getUser(userId);
		model.addAttribute("user", user);
		
		return "mnInformation";
	}

	@RequestMapping(value = "/mnInformation", method = RequestMethod.POST)
	public String mnInformation(String userId, String userEmail, String userPassword, String userNickname
			   , String postcode, String roadAddress, String detailAddress, String extraAddress, String userPhonenum) {
	
		logger.info("mnInformation 메서드(post) 실행.");
		
		
		
		System.out.println(userId);
		System.out.println(userEmail);
		boolean result = service.mnInformation(userId, userEmail, userPassword, userNickname
	    		  , postcode, roadAddress, detailAddress, extraAddress, userPhonenum);
		
		String returnUrl = "";
		if (result) {
			logger.info("데이터 수정 성공.");
			returnUrl = "home";
		} else {
			logger.info("데이터 수정 실패.");
			returnUrl = "mnInformation";
		}
		
		return returnUrl;
	}
	
	@ResponseBody
	@RequestMapping(value = "/kakaoLogin", method = RequestMethod.GET)
	public String kakaoLogin(String kakaoEmail,String kakaoNickname,HttpSession session) {
		
		logger.info("kakaoLogin method get 실행");

		logger.info(kakaoEmail);
		logger.info(kakaoNickname);
		
		boolean result = service.socialIdCheck(kakaoEmail,kakaoNickname);

		String returnUrl = "";

		if (result) {//디비에 아이디가 있음
			logger.info("로그인 성공.");
			session.setAttribute("userId", kakaoEmail);
			session.setAttribute("userNickname", kakaoNickname);
			returnUrl = "redirect:/";
		} else {//디비에 아이디가 없음
			logger.info("아이디가 없습니다. 회원가입 페이지로 넘어갑니다.");
			returnUrl = "socialJoin";
		}
		
		return returnUrl;
	}

	
//	@RequestMapping(value = "/socialJoin", method = RequestMethod.GET)
//	public String socialJoin() {
//		logger.info("socialJoin 메서드(get) 실행.");
//		return "socialJoin";
//	}

	@RequestMapping(value = "/socialJoin", method = RequestMethod.GET)
	public String socialJoin(String userEmail, String userNickname, Model model) {
		logger.info("socialJoin 메서드(get) 실행.");
		logger.info(userEmail);
		logger.info(userNickname);

		model.addAttribute("userId", userEmail);
		model.addAttribute("userNickname", userNickname);

		return "socialJoin";
	}
	
	   @RequestMapping(value = "/star", method = RequestMethod.GET)
	   public String star(int aucNum, Model model) {
		   
		   String aucSellId = itemService.getSellId(aucNum);
		   
		   model.addAttribute("aucSellId", aucSellId);
		   model.addAttribute("aucNum", aucNum);
		   return "star";
	   }
	   
	   @RequestMapping(value = "/star", method = RequestMethod.POST)
	   public String star(String aucSellId, String rating, int aucNum, HttpSession session) {
	      
		   System.out.println(aucSellId);
		   System.out.println(rating);
	      service.insertGrade(aucSellId, rating);
	      
	      System.out.println(aucNum);
	      itemService.updateStatus(aucNum);
	      
	      return "redirect:/myPage";
	   }

}